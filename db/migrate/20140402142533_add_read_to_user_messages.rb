class AddReadToUserMessages < ActiveRecord::Migration
  def change
    add_column :user_messages, :read, :boolean
  end
end
