class CreateUserMessages < ActiveRecord::Migration
  def change
    create_table :user_messages do |t|
			t.string :subject
			t.text :body
			t.references :user
			t.boolean :sent
      t.timestamps
    end
  end
end
