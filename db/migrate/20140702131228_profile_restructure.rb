class ProfileRestructure < ActiveRecord::Migration
  def change
		rename_column :profiles, :pa_gender, :gender
		rename_column :profiles, :pa_age, :age
		rename_column :profiles, :pa_body_type, :body_type
		rename_column :profiles, :pa_eye_color, :eye_color
		rename_column :profiles, :pa_hair_color, :hair_color
		rename_column :profiles, :pa_height, :height
		rename_column :profiles, :pa_race, :ethnicity
		rename_column :profiles, :pa_weight, :weight

		# this is just not used
		remove_column :profiles, :location

		# moving these to user model
		remove_column :profiles, :avatar_id
		remove_column :profiles, :username
		remove_column :profiles, :photos_count

		# remove profile_id from photos as gonna tie photos to user model instead of profile
		remove_column :photos, :profile_id
  end
end
