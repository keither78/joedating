class AddParentIdToUserMessage < ActiveRecord::Migration
  def change
    add_column :user_messages, :parent_id, :integer
  end
end
