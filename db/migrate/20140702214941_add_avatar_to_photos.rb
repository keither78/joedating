class AddAvatarToPhotos < ActiveRecord::Migration
  def change
    add_column :photos, :avatar, :boolean
  end
end
