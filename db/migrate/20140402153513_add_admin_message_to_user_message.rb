class AddAdminMessageToUserMessage < ActiveRecord::Migration
  def change
    add_column :user_messages, :admin_message, :boolean
  end
end
