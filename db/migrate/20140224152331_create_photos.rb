class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|
      t.string :name
      t.integer :profile_id
      t.integer :size

      t.timestamps
    end
  end
end
