class CreateInboxes < ActiveRecord::Migration
  def change
    create_table :inboxes do |t|
      t.string :name
      t.references :user, index: true
      t.timestamps
    end
  end
end
