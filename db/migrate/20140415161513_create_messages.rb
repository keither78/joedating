class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.references :user
      t.integer :sender_id
      t.text :body
      t.boolean :read

      t.timestamps
		end

		add_index 'messages', ['sender_id'], name: 'index_messages_on_sender_id'
  end
end
