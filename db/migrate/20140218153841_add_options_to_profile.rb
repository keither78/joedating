class AddOptionsToProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :pa_gender, :string
    add_column :profiles, :pa_race, :string
    add_column :profiles, :pa_height, :integer, :default => 25
    add_column :profiles, :pa_weight, :integer
    add_column :profiles, :pa_body_type, :string
    add_column :profiles, :pa_hair_color, :string
    add_column :profiles, :pa_eye_color, :string
    add_column :profiles, :pa_age, :integer
  end
end
