class ChangeRoleTypeInUsers < ActiveRecord::Migration
  def change
		execute <<-SQL
			ALTER TABLE users
				ALTER COLUMN role TYPE int USING (role::integer)
		SQL
  end
end
