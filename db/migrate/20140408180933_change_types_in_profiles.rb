class ChangeTypesInProfiles < ActiveRecord::Migration
  def change
		execute <<-SQL
			ALTER TABLE profiles ALTER COLUMN pa_gender TYPE int USING (pa_gender::integer);
			ALTER TABLE profiles ALTER COLUMN pa_race TYPE int USING (pa_race::integer);
			ALTER TABLE profiles ALTER COLUMN pa_body_type TYPE int USING (pa_body_type::integer);
			ALTER TABLE profiles ALTER COLUMN pa_hair_color TYPE int USING (pa_hair_color::integer);
			ALTER TABLE profiles ALTER COLUMN pa_eye_color TYPE int USING (pa_eye_color::integer);
		SQL
  end
end
