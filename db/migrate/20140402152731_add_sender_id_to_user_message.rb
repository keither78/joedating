class AddSenderIdToUserMessage < ActiveRecord::Migration
  def change
    add_column :user_messages, :sender_id, :integer
  end
end
