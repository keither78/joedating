class AddPhotosCountToProfile < ActiveRecord::Migration
  def change
		add_column :profiles, :photos_count, :integer, default: 0, null: false
  end
end
