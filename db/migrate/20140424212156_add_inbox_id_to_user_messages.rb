class AddInboxIdToUserMessages < ActiveRecord::Migration
  def change
    add_reference :user_messages, :inbox, index: true
  end
end
