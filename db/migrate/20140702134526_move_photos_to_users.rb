class MovePhotosToUsers < ActiveRecord::Migration
  def change
		add_column :photos, :user_id, :integer
		add_column :users, :photos_count, :integer
  end
end
