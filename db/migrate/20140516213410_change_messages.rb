class ChangeMessages < ActiveRecord::Migration
  def change
		remove_column :messages, :sender_id
		remove_column :messages, :user_id
		add_reference :messages, :conversation, index: true
  end
end
