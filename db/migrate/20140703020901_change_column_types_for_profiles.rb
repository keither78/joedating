class ChangeColumnTypesForProfiles < ActiveRecord::Migration
  def change
		change_column :profiles, :gender, :string
		change_column :profiles, :ethnicity, :string
		change_column :profiles, :eye_color, :string
		change_column :profiles, :hair_color, :string
		change_column :profiles, :body_type, :string
  end
end
