class AddUrlToPlans < ActiveRecord::Migration
  def change
    add_column :plans, :url, :string
  end
end
