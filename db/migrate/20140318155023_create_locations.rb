class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
			t.string :country_code, limit: 2
			t.string :zipcode, limit: 20
			t.string :city, limit: 180
			t.string :state_name, limit: 100
			t.string :state_abbr, limit: 20
			t.string :county, limit: 100
			t.string :county_code, limit: 20
			t.string :community, limit: 100
			t.string :community_code, limit: 20
			t.float :latitude
			t.float :longitude
			t.integer :accuracy

      t.timestamps
    end
  end
end
