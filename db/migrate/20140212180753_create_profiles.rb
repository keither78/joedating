class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.date :dob
      t.string :title, :default => 'I will tell you later'
      t.text :about
      t.text :iwant
      t.string :location
      t.references :user, index: true

      t.timestamps
    end
  end
end
