class AddVideosCountToUser < ActiveRecord::Migration
  def change
    add_column :users, :videos_count, :integer
  end
end
