class AddPostbacksToSubscription < ActiveRecord::Migration
  def change
    add_column :subscriptions, :method, :string
    add_column :subscriptions, :inv_number, :integer
    add_column :subscriptions, :customer_id, :integer
    add_column :subscriptions, :last_name, :string
    add_column :subscriptions, :first_name, :string
    add_column :subscriptions, :sub_start, :datetime
    add_column :subscriptions, :sub_end, :datetime
    add_column :subscriptions, :product_id, :integer
    add_column :subscriptions, :status, :string
		remove_column :subscriptions, :plan_id
		remove_column :subscriptions, :stripe_customer_token
  end
end
