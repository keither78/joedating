after 'production:users' do
	vip1 = User.find_by_email('vip1@example.com')
	if vip1
		vip1.profile.update_attributes!(title: 'I will tell you later', about: 'I will tell you later', iwant: 'I will tell you later', dob: 27.years.ago.to_date, gender: 'female', ethnicity: 'white', body_type: 'slim', hair_color: 'blonde', eye_color: 'blue')
	end
	vip2 = User.find_by_email('vip2@example.com')
	if vip2
		vip2.profile.update_attributes!(title: 'I will tell you later', about: 'I will tell you later', iwant: 'I will tell you later', dob: 32.years.ago.to_date, gender: 'female', ethnicity: 'white', body_type: 'average', hair_color: 'blonde', eye_color: 'blue')
	end
end