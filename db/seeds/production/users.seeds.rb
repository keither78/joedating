#User.destroy_all

user = CreateAdminService.new.call
puts 'CREATED ADMIN USER: ' << user.email

user1 = User.find_by_email('vip1@example.com')
if user1
	user1.destroy
end
user2 = User.find_by_email('vip2@example.com')
if user2
	user2.destroy
end

vips = [
	{email: 'vip1@example.com', username: 'bucketfir'},
	{email: 'vip2@example.com', username: 'shotputhole'}
]

vips.each do |vip|
	User.create!(email: vip[:email], username: vip[:username], password: CONFIG[:dev_password], password_confirmation: CONFIG[:dev_password], confirmed_at: Time.now, role: 1)
	puts 'VIP USER: ' << vip[:email]
end

