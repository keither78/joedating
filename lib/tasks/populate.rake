namespace :db do
	desc 'Erase and fill database'
	task :populate => :environment do
		require 'ffaker'

		Rake::Task['db:reset'].invoke

		puts "-----------------------"
		puts "RAILS_ENV is #{Rails.env}"
		puts "-----------------------"

		make_admin_user
		make_confirmed_users
		make_unconfirmed_users
	end
end

def make_admin_user
	puts "----------------"
	puts "-- Admin User --"
	puts "----------------"
	User.create!(
		email: 'admin@admin.org',
		password: DEVCONFIG[:dev_password],
		password_confirmation: DEVCONFIG[:dev_password],
		confirmed_at: Time.now,
		role: 'admin'
	)
end

def make_confirmed_users
	puts "---------------------"
	puts "-- Confirmed Users --"
	puts "---------------------"
	50.times do |n|
		email = "user-#{n+1}@confirmed.org"
		password = DEVCONFIG[:dev_password]
		confirmed_user = User.create!(
			email: email,
			password: password,
			password_confirmation: password,
			confirmed_at: Time.now,
			role: 'member'
				# if (n + 1) % 5 == 0
				# 	'vip'
				# else
				# 	'member'
				# end
		)
		update_profile(confirmed_user, n)
	end
end

def make_unconfirmed_users
	puts "-----------------------"
	puts "-- Unconfirmed Users --"
	puts "-----------------------"
	2.times do |n|
		email = "user-#{n+1}@unconfirmed.org"
		password = DEVCONFIG[:dev_password]
		User.create!(
			email: email,
			password: password,
			password_confirmation: password,
			role: 'guest'
		)
	end
end

private

def random_date_between(from, till)
	from = Date.parse(from) unless from.is_a? Date
	till = Date.parse(till) unless till.is_a? Date
	from + rand(till - from)
end

def update_profile(user, n)
	profile = user.profile
	profile.title = Faker::Lorem.sentence(word_count = 20)
	profile.about = Faker::Lorem.paragraphs(paragraph_count = 3).join(' ')
	profile.iwant = Faker::Lorem.paragraphs(paragraph_count = 2).join(' ')
	profile.dob = random_date_between(50.years.ago.to_date, 18.years.ago.to_date)
	profile.pa_gender = %w(Male Female).sample
	profile.pa_race = CONFIG[:races].sample
	profile.pa_body_type = CONFIG[:body_types].sample
	profile.pa_hair_color = CONFIG[:hair_colors].sample
	profile.pa_eye_color = CONFIG[:eye_colors].sample
	if ((n + 1) % 2 == 0)
		add_avatar(profile, n)
	end
	profile.save
end

def add_avatar(profile, n)
	profile.avatar = Photo.create!(profile_id: profile.id, image: File.open(File.join(Rails.root, "public/images/girls/#{n}.jpg")))
	#profile.avatar = avatar
end