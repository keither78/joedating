require 'csv'

namespace :data do
	desc 'Imports a CSV file'
	task :import_csv, [:csv_location, :model] => [:environment] do |t, args|
		return if args.csv_location.nil?
		lines = File.new(args[:csv_location]).readlines
		header = lines.shift.strip
		keys = header.split(',')
		lines.each do |line|
			values = line.strip.split(',')
			attributes = Hash[keys.zip values]
			Module.const_get(args[:model]).create(attributes)
		end
	end

	desc 'Imports users from csv file'
	task :import_users => [:environment] do
		User.destroy_all
		file = "db/users.csv"
		n=1
		CSV.foreach(file, headers: false) do |row|
			dob = fix_date(row[0])
			title = row[1]
			about = row[2]
			iwant = row[3]
			gender = row[4]
			ethnicity = row[5]
			height = row[6]
			weight = row[7]
			body_type = row[8]
			hair_color = row[9]
			eye_color = row[10]
			username = row[11]
			u = User.new
			u.email = "user-#{n}@example.com"
			u.password = CONFIG[:dev_password]
			u.password_confirmation = CONFIG[:dev_password]
			u.role = 'fake'
			u.username = username
			u.confirmed_at = Time.now
			# file = File.join(Rails.root, "public/images/girls/#{n}/1.jpg")
			images = Dir.glob("public/images/girls/#{n}/*.{jpg,JPG}")
			videos = Dir.glob("public/images/girls/#{n}/*.mp4")
			if u.save
				if images
					images.each_with_index do |image,index|
						if index == 0
							u.update(avatar: Photo.create!(user_id: u.id, avatar: true, image: File.open(image))) if File.exists?(image)
						else
							u.photos.create(image: File.open(image))
						end
					end
				end
				if videos
					videos.each do |v|
						u.videos.create(video: File.open(v)) if File.exists?(v)
					end
				end
				do_update_profile(u, dob, title, about, iwant, gender, ethnicity, height, weight, body_type, hair_color, eye_color)
				n += 1
			else
				puts "User not saved, error is #{u.errors}"
			end
		end
		puts "CSV Import Successful #{n} new users added to data base."
	end

	desc 'Imports profiles to match users'
	task :import_profiles => [:environment] do
		file = "db/profiles.csv"
		CSV.foreach(file, headers: true) do |row|
			image = row[13]
			user = User.where(email: row[0]).first
			user.profile.avatar = Photo.create!(profile_id: user.profile.id, avatar: true, image: File.open(File.join(Rails.root, "public/images/girls/#{image}")))
			user.profile.update_attributes!(
				dob: row[1],
				title: row[2],
				about: row[3],
				iwant: row[4],
				pa_gender: row[5],
				pa_race: row[6],
				pa_height: row[7],
				pa_weight: row[8],
				pa_body_type: row[9],
				pa_hair_color: row[10],
				pa_eye_color: row[11],
				username: row[12]
			)
		end
	end
end

private

def fix_date(date)
	date_a = date.split('/').first(2)
	date_b = '19' + date.split('/')[2]
	fixed_date = Date.strptime((date_a.push(date_b)).join('/'), "%m/%d/%Y")
	return fixed_date
end

def do_update_profile(user, dob, title, about, iwant, gender, ethnicity, height, weight, body_type, hair_color, eye_color)
	attributes = {
		dob: dob.to_date,
		title: title,
		about: about,
		iwant: iwant,
		gender: gender.downcase,
		ethnicity: ethnicity.downcase,
		height: height,
		weight: weight,
		body_type: body_type.downcase,
		hair_color: hair_color.downcase,
		eye_color: eye_color.downcase
	}
	user.profile.update(attributes)
end