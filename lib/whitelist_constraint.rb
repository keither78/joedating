class WhitelistConstraint
	def initialize
		@ips = []
		@ips = '208.184.157'
		@ips = '64.124.49'
		@ips = '208.185.234'
		@ips = '204.94.95'
		@ips = '64.124.46'
		@ips = '66.134.237'
		@ips = '64.32.182'
		@ips = '127.0.0.1'
	end

	def matches?(request)
		@ips.include?(request.remote_ip)
	end
end