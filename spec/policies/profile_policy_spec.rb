require 'spec_helper'

describe ProfilePolicy do
	subject { ProfilePolicy }
	permissions :update? do
		before do
			@user = User.new(email: 'test@test.com', password: 'password', password_confirmation: 'password', confirmed_at: Time.now, role: 'member')
		end
		it "should not grant access to profile" do
			subject.should_not permit(@user, Profile.new)
		end

		it "should grant access to profile" do
			subject.should permit(@user, Profile.new(user_id: @user.id))
		end
	end
end