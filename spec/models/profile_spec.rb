require 'spec_helper'

describe Profile do
	context "has data fields" do
		it { should have_db_column(:title).of_type(:string) }
		it { should have_db_column(:dob).of_type(:date) }
		it { should have_db_column(:about).of_type(:text) }
		it { should have_db_column(:iwant).of_type(:text) }
		it { should have_db_column(:location).of_type(:string) }
		it { should have_db_column(:user_id).of_type(:integer) }
		it { should have_db_column(:pa_race).of_type(:string) }
		it { should have_db_column(:pa_gender).of_type(:string) }
		it { should have_db_column(:pa_height).of_type(:integer) }
		it { should have_db_column(:pa_weight).of_type(:integer) }
		it { should have_db_column(:pa_body_type).of_type(:string) }
		it { should have_db_column(:pa_hair_color).of_type(:string) }
		it { should have_db_column(:pa_eye_color).of_type(:string) }
		it { should have_db_column(:pa_age).of_type(:integer) }
	end

	it { should belong_to(:user)}
	it { should validate_presence_of :title }
	it { should ensure_length_of(:title).is_at_least(20).with_message("is too short (minimum is 20 characters)") }
end
