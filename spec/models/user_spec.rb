require 'spec_helper'
require 'cancan/matchers'

describe User do
	it { should have_db_column(:email).of_type(:string) }
	it { should have_db_column(:encrypted_password).of_type(:string) }
	it { should have_db_column(:reset_password_token).of_type(:string) }
	it { should have_db_column(:remember_created_at).of_type(:datetime) }
	it { should have_db_column(:sign_in_count).of_type(:integer)}
	it { should have_db_column(:current_sign_in_at).of_type(:datetime)}
	it { should have_db_column(:last_sign_in_at).of_type(:datetime)}
	it { should have_db_column(:current_sign_in_ip).of_type(:string)}
	it { should have_db_column(:last_sign_in_ip).of_type(:string)}
	it { should have_db_column(:role).of_type(:string)}

	it { should have_one(:profile).dependent(:destroy) }

	describe 'new user' do
		before do
			@user = User.new(email: 'test@test.com', password: 'password', password_confirmation: 'password')
		end

		subject { @user }

		it { should respond_to(:email) }
		it { should respond_to(:password) }
		it { should respond_to(:password_confirmation)}
		it { should respond_to(:friendships) }
		it { should be_valid }

		describe "when email format is invalid" do
			it 'should be invalid' do
				addresses = %w[user@foo,com user_at_foo.org example.user@foo.foo@bar_baz.com foot@bar+baz.com]
				addresses.each do |invalid_address|
					@user.email = invalid_address
					expect(@user).not_to be_valid
				end
			end
		end

		describe "when email is not present" do
			before { @user.email = "" }
			it { should_not be_valid }
		end

		describe "when password is not present" do
			before { @user.password = "" }
			it { should_not be_valid }
		end

		describe "when password_confirmation is not present" do
			before { @user.password_confirmation = "" }
			it { should_not be_valid }
		end

		describe "when email address is already taken" do
			before do
				user_with_same_email = @user.dup
				user_with_same_email.skip_confirmation!
				user_with_same_email.email = @user.email.upcase
				user_with_same_email.save
			end

			it { should_not be_valid }
		end

		describe "when password does not match password_confirmation" do
			before { @user.password_confirmation = "mismatch" }
			it { should_not be_valid }
		end

		describe "when a password that's too short" do
			before { @user.password = @user.password_confirmation = "a" * 5}
			it { should be_invalid }
		end
	end

	describe "that is a confirmed user" do
		before do
			@confirmed_user = create(:confirmed_user)
		end

		subject { @confirmed_user }

		it "can manage profile he owns" do
			profile = create(:profile)
			profile.user = @confirmed_user
			@confirmed_user.can_manage_profile?(profile).should be_true
			@confirmed_user.can_manage_profile?(create(:profile)).should be_false
		end

		it "is the owner of profile" do
			profile = create(:profile)
			profile.user = @confirmed_user
			@confirmed_user.owner_of?(profile).should be_true
			@confirmed_user.owner_of?(create(:profile)).should be_false
		end
	end

	describe ".admin?" do
		before do
			@administrator = create(:admin)
		end
		context "user has admin role" do
			it "return true" do
				@administrator.is_admin?.should be_true
			end
			it "can manage any profile as admin" do
				@administrator.can_manage_profile?(create(:profile)).should be_true
			end
		end
	end

	describe ".moderator?" do
		context "user has asked role" do
			before do
				@moderator = create(:moderator)
			end
			it "return true" do
				@moderator.moderator?.should be_true
			end
		end
	end

	describe ".vip?" do
		context "user has asked role" do
			before do
				@vip = create(:vip)
			end
			it "return true" do
				@vip.is_vip?.should be_true
			end
		end
	end

	describe "abilities" do
		subject(:ability){ Ability.new(user) }
		let(:user) { nil }

		context "when is an admin" do
			let(:user) { create(:admin)}

			it { should be_able_to(:manage, Profile.new)}
			it { should be_able_to(:manage, Photo.new)}
			it { should be_able_to(:manage, Friendship.new)}
		end

		context "when is a user" do
			let(:user) { create(:confirmed_user)}

			it { should be_able_to(:read, Profile.new)}
		end
	end

end
