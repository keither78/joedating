require 'spec_helper'

describe Friendship do
	let(:user) { create(:confirmed_user) }
	let(:friend) { create(:confirmed_user) }

	describe 'when request is sent' do
		it 'should request the friendship' do
			Friendship.request(user, friend)
			Friendship.exists?(user, friend).should be_true
			assert_status user, friend, 'pending'
			assert_status friend, user, 'requested'
		end
	end

	describe 'when request is accepted' do
		it 'should have accepted on both sides' do
			Friendship.request(user, friend)
			Friendship.accept(user, friend)
			assert_status user, friend, 'accepted'
			assert_status friend, user, 'accepted'
		end
	end

	describe 'when request is breakup' do
		it 'should remove the friendship' do
			Friendship.request(user, friend)
			Friendship.breakup(user, friend)
			Friendship.exists?(user, friend).should_not be_true
		end
	end

	private

	def assert_status(suser, sfriend, sstatus)
		friendship = Friendship.find_by_user_id_and_friend_id(suser, sfriend)
		sstatus.should eql(friendship.status)
	end
end
