require 'spec_helper'

describe "user_messages/edit" do
  before(:each) do
    @user_message = assign(:user_message, stub_model(UserMessage))
  end

  it "renders the edit user_message form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", user_message_path(@user_message), "post" do
    end
  end
end
