require 'spec_helper'

describe "photos/new" do
  before(:each) do
    assign(:photo, stub_model(Photo,
      :name => "MyString",
      :profile_id => 1,
      :size => 1
    ).as_new_record)
  end

  it "renders new photo form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", photos_path, "post" do
      assert_select "input#photo_name[name=?]", "photo[name]"
      assert_select "input#photo_profile_id[name=?]", "photo[profile_id]"
      assert_select "input#photo_size[name=?]", "photo[size]"
    end
  end
end
