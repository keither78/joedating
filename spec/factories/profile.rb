FactoryGirl.define do
	factory :profile do
		dob { 21.years.ago }
		title { Faker::Lorem.sentence(word_count = 4) }
		about { Faker::Lorem.paragraph(sentence_count = 3) }
		iwant { Faker::Lorem.paragraph(sentence_count = 3) }
		location { Faker::Address.city }
		user
	end
end