FactoryGirl.define do
	factory :user do
		sequence(:email) { |n| "john#{n}@doe.com"}
		password 'password'
		password_confirmation 'password'
		role 'guest'
	end

	factory :confirmed_user, parent: :user do
		confirmed_at Time.now
		role 'member'
	end

	factory :admin, parent: :confirmed_user do
		role 'admin'
	end

	factory :vip, parent: :confirmed_user do
		role 'vip'
	end

	factory :moderator, parent: :confirmed_user do
		role 'moderator'
	end

end