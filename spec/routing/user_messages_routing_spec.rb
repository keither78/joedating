require "spec_helper"

describe UserMessagesController do
  describe "routing" do

    it "routes to #index" do
      get("/user_messages").should route_to("user_messages#index")
    end

    it "routes to #new" do
      get("/user_messages/new").should route_to("user_messages#new")
    end

    it "routes to #show" do
      get("/user_messages/1").should route_to("user_messages#show", :id => "1")
    end

    it "routes to #edit" do
      get("/user_messages/1/edit").should route_to("user_messages#edit", :id => "1")
    end

    it "routes to #create" do
      post("/user_messages").should route_to("user_messages#create")
    end

    it "routes to #update" do
      put("/user_messages/1").should route_to("user_messages#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/user_messages/1").should route_to("user_messages#destroy", :id => "1")
    end

  end
end
