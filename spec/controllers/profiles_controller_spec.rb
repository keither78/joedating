require 'spec_helper'

describe ProfilesController do
	describe 'Get new' do
		before do
			@user = create(:confirmed_user)
			sign_in @user
		end
		it 'renders the new template' do
			get :new
			expect(response).to render_template('new')
		end
	end
end
