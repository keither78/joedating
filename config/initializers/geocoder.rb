Geocoder.configure(
	lookup: :google,
	ip_lookup: :maxmind_local,
	maxmind_local: {file: File.join('lib/geo', 'GeoIPCity.dat')}
)