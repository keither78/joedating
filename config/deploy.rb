# config valid only for Capistrano 3.1
lock '3.2.1'

set :application, 'randomaffair'
set :deploy_user, 'deployer'

set :scm, :git
set :repo_url, 'git@bitbucket.org:keither78/joedating.git'

set :rbenv_type, :system
set :rbenv_ruby, '2.1.0'
set :rbenv_prefix, "RBENV_ROOT=#{fetch(:rbenv_path)} RBENV_VERSION=#{fetch(:rbenv_ruby)} #{fetch(:rbenv_path)}/bin/rbenv exec"
set :rbenv_map_bins, %w{rake gem bundle ruby rails}

set :keep_releases, 5

set :linked_files, %w{config/database.yml config/config.yml lib/geo/GeoIPCity.dat db/users.csv}

set :linked_dirs, %w{lib/geo tmp/pids tmp/cache tmp/sockets public/images/girls public/system log}

set :tests, [""]

set(:config_files, %w(
	nginx.puma.conf
	config.yml
	database.example.yml
	log_rotation
))

set(:symlinks, [
	{
		source: 'nginx.puma.conf',
		link: "/etc/nginx/sites-enabled/{{full_app_name}}"
	},
	{
		source: 'log_rotation',
		link: "/etc/logrotate.d/{{full_app_name}}"
	}
])

namespace :deploy do
	before :deploy, "deploy:check_revision"
	after 'deploy:symlink:shared', 'deploy:compile_assets_locally'
	after :finishing, 'deploy:cleanup'
end
