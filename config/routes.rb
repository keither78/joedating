Rails.application.routes.draw do

	# root 'pages#index'

	get 'about', to: 'pages#about', as: :about

	get 'privacy', to: 'pages#privacy', as: :privacy

	get 'terms', to: 'pages#terms', as: :terms

	get 'compliance', to: 'pages#compliance', as: :compliance

	get 'random_user', to: 'users#get_random_user', as: :random_user

	get 'check_username', to: 'pages#check_username', as: :check_username

	get 'check_email', to: 'pages#check_email', as: :check_email

	match 'validate_user', to: 'pages#validate_user', via: :post, as: :validate_user

	match 'search', to: 'users#search', via: [:get, :post], as: :user_search
	match 'plans/receive', to: 'plans#receive', via: [:get, :post], defaults: { format: 'json' }, constraints: WhitelistConstraint.new
	get '/photos/:id/images', to: 'users#get_image'
	get '/videos/:id/videos', to: 'users#get_video'

	authenticated :user do
		root to: 'users#user_dashboard', as: :authenticated_root
	end

	unauthenticated do
		root 'pages#index'
	end

	concern :photogenic do
		resources :photos
	end

	resources :users do
		get 'user_dashboard', on: :member
		get 'online_users', on: :collection
		concerns :photogenic
		resources :videos
		resources :profiles
	end

	resources :inboxes
	resources :user_messages do
		get 'sent_messages', on: :collection
	end
	resources :conversations
	resources :subscriptions
	resources :plans, only: [:index]

	devise_for :users, :path => 'accounts', controllers: { confirmations: 'confirmations', sessions: 'sessions', registrations: 'registrations' }

	resources :messages
	resources :friendships

	namespace :admin do
		root 'dashboard#index'
		resources :users, only: [:index, :show]
		resources :user_messages
		resources :plans
	end
end
