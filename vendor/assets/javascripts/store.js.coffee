# Storage API
@store = (->
	serialize = (value) ->
		JSON.stringify value
	deserialize = (value) ->
		return `undefined`  unless typeof value is "string"
		JSON.parse value
	api = {}
	win = window
	doc = win.document
	sessionStorageName = "sessionStorage"
	localStorageName = "localStorage"
	globalStorageName = "globalStorage"
	storage = undefined
	api.set = (key, value) ->

	api.get = (key) ->

	api.remove = (key) ->

	api.clear = ->

	if sessionStorageName of win and win[sessionStorageName]
		storage = win[sessionStorageName]
		api.set = (key, val) ->
			storage[key] = serialize(val)
			return

		api.get = (key) ->
			deserialize storage[key]

		api.remove = (key) ->
			delete storage[key]

			return

		api.clear = ->
			storage.clear()
			return
	else if localStorageName of win and win[localStorageName]
		storage = win[localStorageName]
		api.set = (key, val) ->
			storage[key] = serialize(val)
			return

		api.get = (key) ->
			deserialize storage[key]

		api.remove = (key) ->
			delete storage[key]

			return

		api.clear = ->
			storage.clear()
			return
	else if globalStorageName of win and win[globalStorageName]
		storage = win[globalStorageName][win.location.hostname]
		api.set = (key, val) ->
			storage[key] = serialize(val)
			return

		api.get = (key) ->
			deserialize storage[key] and storage[key].value

		api.remove = (key) ->
			delete storage[key]

			return

		api.clear = ->
			for key of storage
				delete storage[key]
			return
	else if doc.documentElement.addBehavior
		getStorage = ->
			return storage  if storage
			storage = doc.body.appendChild(doc.createElement("div"))
			storage.style.display = "none"

			# See http://msdn.microsoft.com/en-us/library/ms531081(v=VS.85).aspx
			# and http://msdn.microsoft.com/en-us/library/ms531424(v=VS.85).aspx
			storage.addBehavior "#default#userData"
			storage.load localStorageName
			storage
		api.set = (key, val) ->
			storage = getStorage()
			storage.setAttribute key, serialize(val)
			storage.save localStorageName
			return

		api.get = (key) ->
			storage = getStorage()
			deserialize storage.getAttribute(key)

		api.remove = (key) ->
			storage = getStorage()
			storage.removeAttribute key
			storage.save localStorageName
			return

		api.clear = ->
			storage = getStorage()
			attributes = storage.XMLDocument.documentElement.attributes
			storage.load localStorageName
			i = 0
			attr = undefined

			while attr = attributes[i]
				storage.removeAttribute attr.name
				i++
			storage.save localStorageName
			return
	api
)()