# == Schema Information
#
# Table name: subscriptions
#
#  id                    :integer          not null, primary key
#  plan_id               :integer
#  email                 :string(255)
#  stripe_customer_token :string(255)
#  created_at            :datetime
#  updated_at            :datetime
#

class Subscription < ActiveRecord::Base
	belongs_to :user
	#belongs_to :plan
	#validates :plan_id, presence: true
	validates :email, presence: true

	def setup_subscription(user = nil, params)
		self.user = user
		self.email = params['CUST_EMAIL']
		self.method = params['METHOD']
		self.inv_number = params['INV_ID']
		self.customer_id = params['CUST_ID']
		self.last_name = params['CUST_LNAME']
		self.first_name = params['CUST_FNAME']
		self.sub_start = params['INV_DATE']
		self.sub_end = get_duration(params['PROD_ID'], params['INV_DATE'])
		self.product_id = params['PROD_ID']
		self.status = params['SUB_STATUS']
		self.save!
	end

	def cancel_sub(user, params)
		self.cancel_date = Time.now
		self.status = params['SUB_STATUS']
		self.save!
	end

	def suspend_sub(user, params)
		self.status = params['SUB_STATUS']
		self.save!
	end

	def renew_sub(user, params)
		self.status = params['SUB_STATUS']
		self.sub_end = get_duration(params['PROD_ID'], params['INV_DATE'])
	end

	def void_sub(user, params)
		self.status = params['SUB_STATUS']
	end

	#attr_accessor :stripe_card_token

	# def save_with_payment
	# 	if valid?
	# 		customer = Stripe::Customer.create(description: email, plan: plan_id, card: stripe_card_token)
	# 		self.stripe_customer_token = customer.id
	# 		save!
	# 	end
	# rescue Stripe::InvalidRequestError => e
	# 	logger.error "Stripe error while creating customer: #{e.message}"
	# 	errors.add :base, 'There was a problem with your credit card.'
	# 	false
	# end

	private

	def get_duration(product, start)
		starttime = start.to_datetime

		case product
			when '100065928' # Gold 1 Day membership
				end_date = starttime + 1.day
			when '100065956' # Gold 1 month Membership
				end_date = starttime + 1.month
			when '100065953' # Gold 1 year membership
				end_date = starttime + 12.months
			when '100065955' # Gold 3 months membership
				end_date = starttime + 3.months
			else
				end_date = Time.now + 1.day
		end
		end_date
	end
end
