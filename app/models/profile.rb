class Profile < ActiveRecord::Base
  belongs_to :user
	before_save :set_age

	validates :title, presence: true, length: { minimum: 20 }, on: :update
	validates_associated :user

	#has_many :photos, dependent: :destroy
	#belongs_to :avatar, class_name: 'Photo', foreign_key: 'avatar_id', inverse_of: :profile_as_avatar

	# enum gender: [:male, :female]
	# enum race: [:white, :hispanic_latino, :asian, :african_american]
	# enum body_type: [:slim, :athletic, :average_size, :heavy]
	# enum eye_color: [:blue, :brown, :green, :hazel, :gray]
	# enum hair_color: [:auburn, :blonde, :black, :brunette]

	GENDERS = ['male', 'female']
	ETHICITYS = ['white', 'hispanic_latino', 'asian', 'african_american']
	BODYTYPES = ['slim', 'athletic', 'average', 'heavy']
	EYECOLORS = ['blue', 'brown', 'green', 'hazel', 'gray']
	HAIRCOLORS = ['auburn', 'red', 'black', 'brunette', 'brown', 'blonde']

	#accepts_nested_attributes_for :avatar

	# TODO add photo_count to keep track of number of photos will minimal db query

	def height_in_feet
		(height/12).floor.to_s+'\''+(height%12).to_s
	end

	# def avatar_photo_url(size = :original)
	# 	if avatar_id
	# 		avatar.image.url(size)
	# 	else
	# 		case size
	# 			when :thumb
	# 				'/images/thumb_missing.png'
	# 			else
	# 				'/images/missing.png'
	# 		end
	# 	end
	# end

	def self.random_profile
		# with_avatar.limit(1).order("RANDOM()").first()
		limit(1).order("RANDOM()").first()
	end

	def self.with_avatar
		where.not(avatar_id: nil)
	end

	def self.has_at_least_one_photo
		where.not(photos_count: nil)
	end

	private

	def set_age
		now = Date.today
		if self.dob
			self.age = now.year - self.dob.year - (now.strftime('%m%d') < self.dob.strftime('%m%d') ? 1 : 0)
		end
	end

end
