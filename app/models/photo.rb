class Photo < ActiveRecord::Base
	after_save :update_avatars
	belongs_to :user, counter_cache: true

	Paperclip.interpolates :user_id do |attachment, style|
		"user_#{attachment.instance.user_id.to_s}"
	end

	attachment_virtual_path = "/system/attachments/:rails_env/:hashed_path/:user_id/images/:id/:style/:basename.:extension"
	attachment_real_path = ":rails_root/public" + attachment_virtual_path

	has_attached_file :image,
										:styles => { :original => '800x800#', :tiny => '50x50#', :thumb => '150x150#', :medium => '350x350#'},
										:source_file_options => { :all => '-auto-orient' },
										:path => attachment_real_path,
										:url => attachment_virtual_path

	validates :image, presence: true, on: :create
	validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
	protected

	def update_avatars
		self.class.where('user_id = ? AND avatar = ? AND id != ?', user_id, true, id).update_all(avatar: false) if avatar?
	end

end
