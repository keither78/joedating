# == Schema Information
#
# Table name: locations
#
#  id             :integer          not null, primary key
#  country_code   :string(2)
#  zipcode        :string(20)
#  city           :string(180)
#  state_name     :string(100)
#  state_abbr     :string(20)
#  county         :string(100)
#  county_code    :string(20)
#  community      :string(100)
#  community_code :string(20)
#  latitude       :float
#  longitude      :float
#  accuracy       :integer
#  created_at     :datetime
#  updated_at     :datetime
#

class Location < ActiveRecord::Base
	geocoded_by :zipcode
	#after_validation :geocode
	#
	#validates :zipcode, presence: true, format: { with: %r{\d{5}(-\d{4})?} }
end
