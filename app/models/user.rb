class User < ActiveRecord::Base
	include Concerns::Users::OnlineStatus
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable, :confirmable, :timeoutable

	before_create :build_associations, :generate_channel_key
	after_initialize :set_default_role, if: :new_record?

	has_one :profile, dependent: :destroy
	has_one :inbox, dependent: :destroy
	has_one :subscription, dependent: :destroy

	has_one :avatar, -> { where(avatar: true) }, class_name: 'Photo'

	has_many :photos, dependent: :destroy
	has_many :videos, dependent: :destroy
	has_many :friendships, dependent: :destroy
	has_many :friends, -> { where("status = 'accepted'") }, through: :friendships
	#has_many :requested_friends, -> { where("status = 'requested'") }, through: :friendships, source: :friend
	#has_many :pending_friends, -> { where("status = 'pending'") }, through: :friendships, source: :friend

	has_many :user_messages, inverse_of: :user
	has_many :messages, dependent: :destroy
	has_many :sent_messages, class_name: 'Message', foreign_key: 'sender_id'
	has_many :sent_user_messages, class_name: 'UserMessage', foreign_key: 'sender_id'

	enum role: [:member, :vip, :admin, :fake]

	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

	validates :email, presence: true, uniqueness: true, format: { with: VALID_EMAIL_REGEX }
	validates :username, presence: true, uniqueness: true

	scope :confirmed, -> { where('confirmed_at IS NOT NULL') }
	scope :unconfirmed, -> { where('confirmed_at IS NULL') }
	scope :all_except, -> (user) { where.not(id: user) }

	def can_view?
		vip? || admin?
	end

	def is_admin?
		admin?
	end

	def is_vip?
		vip?
	end

	def is_member?
		member?
	end

	def self.members_and_vips
		where(role: [User.roles[:member], User.roles[:vip], User.roles[:fake]])
	end

	def self.non_admin_users
		where.not(role: User.roles[:admin])
	end

	def self.between_dates(from, to)
		where(:created_at => from..to)
	end

	def self.has_avatar
		where(photos: { avatar: true })
	end

	def owner_of?(object)
		object.user == self
	end

	def can_manage_profile?(profile)
		is_admin? || profile.user == self
	end

	def profile
		super || build_profile
	end

	def inbox
		super || build_inbox(name: 'Main')
	end

	def check_friendships(friend)
		friendship = has_friendship_for(friend)
		friendship
	end

	def friends?(friend)
		friendship = has_friendship_for(friend)
		!!(friendship && friendship.status == 'accepted')
	end

	def has_friendship_for(friend)
		friendships.where(friend_id: friend.id).first
	end

	def after_confirmation
		MainMailer.welcome_from_admin(self).deliver
	end

	def avatar_photo_url(size = :original)
		avatar ?
			avatar.image.url(size) :
			case size
				when :thumb
					'/images/thumb_missing.png'
				else
					'/images/missing.png'
			end
	end

	def self.validate_field(field, value)
		validity = User.new(field => value)
		validity.valid?
		if validity.errors.include?(field.to_sym)
			ajax_response = { valid: false, field.to_sym => validity.errors[field], status: 400 }
		else
			ajax_response = { valid: true, status: 200 }
		end
		return ajax_response
	end

	private

	def build_associations
		profile || true
		inbox || true
	end

	def set_default_role
		self.role ||= :member
	end

	def generate_channel_key
		begin
			key = SecureRandom.urlsafe_base64
		end while User.where(:channel_key => key).exists?
		self.channel_key = key
	end

	ransacker :confirmed_at do
		Arel.sql('date(confirmed_at)')
	end

end