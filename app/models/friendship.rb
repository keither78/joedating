# == Schema Information
#
# Table name: friendships
#
#  id          :integer          not null, primary key
#  user_id     :integer
#  friend_id   :integer
#  status      :string(255)
#  accepted_at :datetime
#  created_at  :datetime
#  updated_at  :datetime
#

class Friendship < ActiveRecord::Base

	belongs_to :user
	belongs_to :friend, class_name: 'User', foreign_key: 'friend_id'

	validates :user_id, :friend_id, presence: true


	after_create do |p|
		Friendship.create!(user_id: p.friend_id, friend_id: p.user_id, status: 'pending') if !Friendship.where(friend_id: p.user_id, user_id: p.friend_id).first
	end

	after_destroy do |p|
		remove_other_side(p.user_id, p.friend_id)
	end

	after_update do |p|
		reciprocal = Friendship.where(friend_id: p.user_id, user_id: p.friend_id).first
		update_other_side_of_friendship(reciprocal, p.friend_id, p.user_id)
	end

	def self.accepted
		where(status: 'accepted')
	end

	def self.pending
		where(status: 'pending')
	end

	def self.requested
		where(status: 'requested')
	end

	private

	def remove_other_side(user_id, friend_id)
		reciprocal = Friendship.where(friend_id: user_id).first
		unless reciprocal.nil?
			reciprocal.destroy
			User.decrement_counter(:friends_count, user_id)
			User.decrement_counter(:friends_count, friend_id)
		end
	end

	def update_other_side_of_friendship(friendship, friend_id, user_id)
		unless friendship.status == 'accepted'
			friendship.update_attribute(:status, 'accepted')
			User.increment_counter(:friends_count, user_id)
			User.increment_counter(:friends_count, friend_id)
		end
	end
end
