# TODO need to add a messages_count to users model and add logic so no need to use sql count

class UserMessage < ActiveRecord::Base
	belongs_to :user
	belongs_to :sender, :class_name => "User", :foreign_key => "sender_id"
	belongs_to :inbox, inverse_of: :user_messages

	has_many :children, class_name: 'UserMessage', foreign_key: 'parent_id'
	belongs_to :parent, class_name: 'UserMessage', foreign_key: 'parent_id'

	validates :body, :user, presence: true

	scope :sent, -> { where sent: true }
	scope :received, -> { where sent: false }
	scope :unread, -> { where read: false }
	scope :fake_user_messages, -> { includes(:user, {:sender => :inbox}).where(read: false).where('users.role = ?', 3).references(:user) }

	#def add_user_message(from, recipient, admin=false)
	def add_user_message(sent=true, admin=false)
		# TODO perhaps later we will need a sent
		#msg = self.dup
		#msg.sent = false
		#msg.user_id = recipient.id
		#msg.save
		#self.update_attributes(user_id: from.id, sent: true, read: false)
		#self.update_attributes!(user_id: recipient.id, sender_id: from.id, sent: false, read: false, admin_message: admin)
		self.update_attributes!(sent: sent, read: false, admin_message: admin)
	end

	def is_admin_message?
		self.admin_message?
	end

	def mark_as_read
		self.update_attributes!(read: true)
		if self.children.count > 0
			self.children.includes(:user).each do |child|
				child.update_attributes!(read: true)
			end
		end
	end

	def mark_as_unread
		self.update_attributes!(read: false)
	end

	def from
		User.find(self.sender_id)
	end

end
