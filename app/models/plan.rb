# == Schema Information
#
# Table name: plans
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  price      :decimal(, )
#  created_at :datetime
#  updated_at :datetime
#

class Plan < ActiveRecord::Base
	#has_many :subscriptions

	validates :name, :price, presence: true
	validates :price, numericality: {greater_than_or_equal_to: 0.01}
end
