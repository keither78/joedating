class Conversation < ActiveRecord::Base
	belongs_to :sender, :class_name => "User", :foreign_key => "sender_id"
	belongs_to :recipient, :class_name => "User", :foreign_key => "recipient_id"
	has_many :messages, dependent: :destroy

	accepts_nested_attributes_for :messages, allow_destroy: true, reject_if: proc { |message| message[:body].blank? }
end
