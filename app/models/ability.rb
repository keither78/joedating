# # TODO Should move to pundit
# class Ability
#   #include CanCan::Ability
#
#   def initialize(user)
# 		user ||= User.new
#
# 		if user.is_admin?
# 			can :manage, :all
# 		elsif user.is_vip?
# 			can :read, User
# 			can :read, Photo
# 			can :get_image, Photo
# 			can :create, Photo, :user => { :id => user.id }
# 			can :read, Profile
# 			can [:update, :destroy], Profile, :user => { :id => user.id }
# 			can :read, Video
# 			can :create, Video, :user => { :id => user.id }
# 			can [:update, :destroy], Video, :user => { :id => user.id }
# 			can :read, Inbox, :user_id => user.id
# 			can :sent_messages, UserMessage, :user_id => user.id
# 			can :read, UserMessage do |um|
# 				is_user_message_readable?(user, um)
# 			end
# 			can :create, UserMessage
# 			can [:update, :destroy], UserMessage, :user_id => user.id
# 		elsif user.is_member?
# 			can :read, User
# 			can :read, Photo
# 			can :get_image, Photo
# 			can :create, Photo, :user => { :id => user.id }
# 			can :read, Profile
# 			can [:update, :destroy], Profile, :user => { :id => user.id }
# 			can :read, Video
# 			can :read, Inbox, :user_id => user.id
# 			can :sent_messages, UserMessage, :user_id => user.id
# 			can :read, UserMessage do |um|
# 				is_user_message_readable?(user, um)
# 			end
# 		else
# 			can :read, User
# 			can :read, Photo
# 			can :get_image, Photo
# 		end
# 	end
#
# 	def is_message_readable?(user, message)
# 		unless message
# 			raise CanCan::AccessDenied.new('No such message!', :read, Message)
# 		end
#
# 		unless message.try(:user) == user
# 			raise CanCan::AccessDenied.new('You cannot view a message that does not belong to you', :read, Message)
# 		end
#
# 		return true
# 	end
#
# 	def is_user_message_readable?(user, message)
# 		unless message
# 			raise CanCan::AccessDenied.new('No such message!', :read, UserMessage)
# 		end
#
# 		unless message.try(:user) == user || message.sender_id == user.id
# 			raise CanCan::AccessDenied.new('You cannot view a message that does not belong to you', :read, UserMessage)
# 		end
#
# 		unless message.is_admin_message? || user.is_vip?
# 			raise CanCan::AccessDenied.new('You have to have a subscription to view this message', :read, UserMessage)
# 		end
#
# 		return true
# 	end
# end
