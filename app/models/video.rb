class Video < ActiveRecord::Base
	belongs_to :user, counter_cache: true

	Paperclip.interpolates :user_id do |attachment, style|
		"user_#{attachment.instance.user_id.to_s}"
	end

	attachment_virtual_path = "/system/attachments/:rails_env/:hashed_path/:user_id/videos/:id/:style/:basename.:extension"
	attachment_real_path = ":rails_root/public" + attachment_virtual_path

	has_attached_file :video,
										:styles => { :thumb => { :geometry => '50x50#', :format => 'jpg'} },
										:processors => [:ffmpeg],
										:path => attachment_real_path,
										:url => attachment_virtual_path

	validates :video, presence: true, on: :create
	validates_attachment_content_type :video, :content_type => /\Avideo\/.*\Z/
end
