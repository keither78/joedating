class MainMailer < ActionMailer::Base
  #default from: "from@example.com"

	def friend_request(user, friend)
		@user = user
		@friend = friend
		@user_url = profile_url(@user)
		mail(to: @friend.email, subject: "New friend request from #{@user.username}")
	end

	def welcome_from_admin(user)
		@user = user
		mail(to: @user.email, subject: "Welcome to Randomaffair #{@user.username}")
	end

	def user_sent_inbox_message(recipient, sender)
		@recipient = recipient
		@sender = sender
		@sender_url = user_profile_url(@sender, @sender.profile)
		@url = 'http://www.randomaffair.com'
		mail(to: @recipient.email, subject: "New message from #{@sender.username}")
	end

	def send_message_to_fake_user_to_admin(message, sender)
		@message = message
		@sender = sender
		mail(to: 'admin@randomaffair.com', subject: "#{@sender.username} sent a message to #{@message.user.username}")
	end
end
