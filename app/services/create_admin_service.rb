class CreateAdminService
	def call
		user = User.find_or_create_by!(email: CONFIG[:admin_email]) do |user|
			user.password = CONFIG[:admin_password]
			user.password_confirmation = CONFIG[:admin_password]
			user.username = 'admin'
			user.admin!
			# user.skip_confirmation!
			user.confirmed_at = Time.now
		end
	end
end