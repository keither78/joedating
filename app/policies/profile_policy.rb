class ProfilePolicy < ApplicationPolicy
  class Scope < Struct.new(:user, :scope)
    def resolve
      scope
    end
	end

	def update?
		user.is_admin? or record.user == user
	end
end
