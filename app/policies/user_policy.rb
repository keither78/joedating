class UserPolicy < ApplicationPolicy
  class Scope < Struct.new(:user, :scope)
    def resolve
      scope
    end
	end

	def show?
		user.is_admin? or record == user
	end

	def add_photo?
		record == user
	end

	def add_video?
		record == user
	end
end
