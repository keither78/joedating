class UserMessagePolicy < ApplicationPolicy
  class Scope < Struct.new(:user, :scope)
    def resolve
      scope
    end
	end

	def show?
		(scope.where(:id => record.id).exists? && (user.is_vip? || record.is_admin_message?) && (record.user == user || record.sender == user))
	end

	def sent_messages?
		record.sender.id == user.id
	end

	def create?
		user.is_admin? or user.is_vip?
	end
end
