class InboxPolicy < ApplicationPolicy
  class Scope < Struct.new(:user, :scope)
    def resolve
      scope
    end
	end

	def index?
		user.is_admin? or record.user == user
	end

	def show?
		user.is_admin? or record.user == user
	end
end
