class ConversationPolicy < ApplicationPolicy
  class Scope < Struct.new(:user, :scope)
    def resolve
      scope
    end
	end

	def create?
		user.is_admin? or user.is_vip?
	end
end
