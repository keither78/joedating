class PhotoPolicy < ApplicationPolicy
  class Scope < Struct.new(:user, :scope)
    def resolve
      scope
    end
	end

	def index?
		true
	end

	def create?
		record.user == user
	end

	def update?
		record.user == user
	end

	def destroy?
		user.is_admin? or record.user == user
	end
end
