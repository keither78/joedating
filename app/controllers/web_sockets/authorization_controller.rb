class WebSockets::AuthorizationController < WebsocketRails::BaseController

	# def authorize_channels
	# 	channel = WebsocketRails[message[:channel]]
	# 	if can? :subscribe, channel
	# 		accept_channel current_user
	# 	else
	# 		deny_channel({:message => 'authorization failed!'})
	# 	end
	# end

	def get_channel_key
		if user_signed_in?
			key = current_user.channel_key
			WebsocketRails[key].make_private
			send_message :key, key, :namespace => :user
		else
			send_message :key, nil, :namespace => :user
		end
	end

	def authorize_user_channel
		if user_signed_in? and current_user.channel_key == message[:channel]
			if current_user.has_been_seen == 1
				WebsocketRails[:online_users].trigger 'has_been_seen', current_user
			end
			accept_channel nil
		else
			deny_channel nil
		end
	end

	def client_disconnected
		WebsocketRails[:online_users].trigger 'has_left', current_user if current_user.has_left <= 0
	end
end