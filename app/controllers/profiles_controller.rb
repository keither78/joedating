class ProfilesController < BaseController
	# load_and_authorize_resource :user
	# load_and_authorize_resource :profile, :through => :user, :singleton => true, param_method: :profile_params
	before_filter :load_user_and_profile, only: [:show, :edit, :update]
	after_action :verify_authorized

	def index
		@profiles = Profile.all
	end

	def new
		@profile = current_user.build_profile
		authorize @profile
	end

	def create
		@profile = current_user.build_profile(profile_params)
		respond_to do |format|
			if @profile.save
				format.html { redirect_to root_path, notice: 'Profile created successfully' }
			else
				format.html { render action: 'new' }
			end
		end
	end

	def show
		authorize @profile
		unless current_user.is_vip?
			@plans = Plan.all
		end
	end

	def edit
		authorize @profile
	end

	def update
		authorize @profile
		respond_to do |format|
			if @profile.update(profile_params)
				format.html { redirect_to [@user, @profile], notice: 'Profile updated successfully' }
			else
				format.html { render :edit }
			end
		end
	end


	private

	def profile_params
		params.require(:profile).permit(:user_id, :title, :about, :dob, :iwant, :location, :gender, :ethnicty, :weight, :height, :body_type, :hair_color, :eye_color)
	end

	def load_user_and_profile
		@user = User.find(params[:user_id])
		@profile = @user.profile
	end

end
