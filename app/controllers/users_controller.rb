class UsersController < BaseController
	#TODO make sure to use authorization on this controller (CanCan)

	def index
		@users = User.all
	end

	def show
		@user = User.find(params[:id])
		authorize @user
	end

	def edit
		@user = User.find(params[:id])
		authorize @user
	end

	def update

	end

	# TODO need to add a function to where you can search by only users that have avatar
	def search
		@location = Location.find_by_city(session[:user_info].city_name)
		@locations = Location.near([@location.latitude, @location.longitude], 20)
		@search = User.members_and_vips.all_except(current_user).includes([:profile, :avatar]).search(params[:q])
		@users = @search.result(distinct: true).page(params[:page]).per_page(12)
	end

	def user_dashboard
		@users = User.members_and_vips.all_except(current_user).includes([:profile, :avatar]).has_avatar.limit(8)
		@online_users = User.members_and_vips.all_except(current_user).includes([:profile, :avatar]).has_avatar.limit(8).reverse_order
	end

	def online_users
		@users = User.members_and_vips.all_except(current_user).includes(:profile, :avatar).has_avatar.reverse_order
		@location = Location.find_by_city(session[:user_info].city_name)
		@locations = Location.near([@location.latitude, @location.longitude], 20)
	end

	def get_random_user
		@profile = Profile.random_profile
		cookies[:messaging_user] = @profile.user.username
		respond_to do |format|
			format.js { render partial: 'messages/chatbox', locals: { profile: @profile }}
		end
	end

	def get_image
		@photo = Photo.find(params[:id])
		style = params[:style] ? params[:style] : 'original'
		send_file @photo.image.path(style), type: @photo.image_content_type, disposition: 'inline'
		authorize! :get_image, @photo
	end

	def get_video
		@video = Video.find(params[:id])
		if params[:style]
			style = params[:style]
		else
			style = 'original'
		end
		send_file @video.video.path(style), type: @video.video_content_type, disposition: 'inline'
	end

	private
	# TODO make this controller use strong parameters
	def user_params
		params.require(:q).permit()
	end
end
