class ConversationsController < BaseController
	def index
		@recipient = User.find(params[:user_id])
		@conversation = Conversation.where(sender: [current_user, @recipient], recipient: [@recipient, current_user]).first
		if !@conversation
			@conversation = Conversation.create!(sender: current_user, recipient: @recipient)
		end
		@messages = @conversation.messages
	end

	def new
		@recipient = User.find(params[:user_id])
		@conversation = Conversation.where(sender: [current_user, @recipient], recipient: [@recipient, current_user]).first
		if !@conversation
			@conversation = Conversation.create(sender: current_user, recipient: @recipient)
		end
		@messages = @conversation.messages
		respond_to do |format|
			format.js { render partial: 'messages/chatbox', locals: { profile: @recipient.profile }}
			#format.js { render json: @conversation.to_json(include: {recipient: { include: {profile: {only: :username}}}})}
		end
	end

	def create
		@recipient = User.find(params[:user_id])
		@conversation = Conversation.where(sender: [current_user, @recipient], recipient: [@recipient, current_user]).first
		if !@conversation
			@conversation = Conversation.create(sender: current_user, recipient: @recipient)
		end
		@messages = @conversation.messages
		respond_to do |format|
			format.js { render partial: 'messages/chatbox', locals: { profile: @recipient.profile }}
		end
	end

	def edit
		@recipient = User.find(params[:user_id])
		@conversation = Conversation.where(sender: [current_user, @recipient], recipient: [@recipient, current_user]).first
		@conversation.messages.build
	end

	def update
		@conversation = Conversation.find(params[:id])
		respond_to do |format|
			if @conversation.update(conversation_params)
				WebsocketRails[:conversations].trigger 'update', @conversation
				format.html { redirect_back_or_default(default) }
				format.js
			end
		end
	end

	private

	def conversation_params
		params.require(:conversation).permit(:sender_id, :recipient_id, messages_attributes: [:body, :user_id])
	end
end
