class BaseController < ApplicationController
	before_filter :authenticate_user!

	include Pundit

	rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

	private

	def user_not_authorized(exception)
		policy_name = exception.policy.class.to_s.underscore

		error = I18n.t "pundit.#{policy_name}.#{exception.query}", default: 'You cannot perform this action.'
		if current_user
			redirect_to user_dashboard_user_path(current_user), alert: error
		else
			redirect_to (request.referrer || root_path), alert: error
		end
	end

end