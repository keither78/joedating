class Admin::DashboardController < Admin::BaseController

	def index
		@total_users = User.non_admin_users.size
		@confirmed_users = User.non_admin_users.confirmed.size
		@yesterday_new_users = User.non_admin_users.between_dates(1.day.ago.midnight, Date.today.midnight).size
		@today_new_users = User.non_admin_users.between_dates(Date.today.midnight, Date.today.tomorrow.midnight).size
	end
end