class Admin::PlansController < Admin::BaseController
	before_action :set_plan, only: [:edit, :update, :destroy]

	def index
		@plans = Plan.all
	end

	def new
		@plan = Plan.new
	end

	def edit
	end

	def create
		@plan = Plan.new(plan_params)
		respond_to do |format|
			if @plan.save
				format.html { redirect_to admin_plans_path, notice: 'Plan was created successfully.' }
			else
				format.html { render action: 'new' }
			end
		end
	end

	def update
		respond_to do |format|
			if @plan.update(plan_params)
				format.html { redirect_to admin_plans_path, notice: 'Plan was successfully updated.'}
			else
				format.html { render action: 'edit' }
			end
		end
	end

	def destroy
		@plan.destroy
		respond_to do |format|
			format.html { redirect_to admin_plans_path, notice: 'Plan was successfully removed.' }
		end
	end

	private

	def set_plan
		@plan = Plan.find(params[:id])
	end

	def plan_params
		params.require(:plan).permit(:name, :price, :url, :p_api_id)
	end
end