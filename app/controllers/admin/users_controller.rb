class Admin::UsersController < Admin::BaseController

	def index
		@search = User.includes(:avatar).search(params[:q])
		@users = @search.result.page(params[:page]).per_page(10)
		#@users = User.non_admin_users.all_except(current_user).includes(:profile => :avatar).page(params[:page]).per_page(10)
	end
end