class Admin::UserMessagesController < Admin::BaseController
	before_filter :load_inbox, only: [:show, :create]
	def index
		@user_messages = UserMessage.fake_user_messages.order(created_at: :desc)
	end

	def show
		@user_message = UserMessage.find(params[:id])
		@user_message.mark_as_read
	end

	def new
		def new
			@user_message = UserMessage.new
			@recipient = User.find(params[:recipient_id])
		end
	end

	def create
		@user_message = @inbox.user_messages.new(user_message_params)
		respond_to do |format|
			if @user_message.add_user_message(false)
				format.html { redirect_to admin_user_messages_path, notice: 'Message was successfully created.' }
				format.json { render action: 'show', status: :created, location: @user_message }
			else
				format.html { render action: 'new' }
				format.json { render json: @user_message.errors, status: :unprocessable_entity }
			end
		end
	end

	private

	def load_inbox
		@inbox = Inbox.find(params[:inbox_id])
	end

	# Never trust parameters from the scary internet, only allow the white list through.
	def user_message_params
		#params[:user_message]
		params.require(:user_message).permit(:sender_id, :body, :subject, :user_id, :parent_id)
	end
end