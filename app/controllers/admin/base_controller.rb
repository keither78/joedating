class Admin::BaseController < BaseController
	layout 'admin'

	before_filter :admin_required

	private

	def admin_required
		redirect_to root_url, alert: 'Access is denied' unless current_user && current_user.is_admin?
	end

end