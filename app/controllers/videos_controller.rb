class VideosController < BaseController
	before_filter :load_user

	def index
		@videos = @user.videos
		authorize @videos
	end

	def show
		@video = @user.videos.find(params[:id])
		authorize @video
		respond_to do |format|
			format.js {}
		end
	end

	def new
		@video = @user.videos.new
		authorize @video
	end

	def create
		@video = @user.videos.new(video_params)
		authorize @video
		respond_to do |format|
			if @video.save
				format.html { redirect_to [@user, @video], notice: 'Video was created successfully'}
			else
				format.html { render action: 'new' }
			end
		end
	end

	def edit
		@video = @user.videos.find(params[:id])
		authorize @video
	end

	def update
		@video = @user.videos.find(params[:id])
		authorize @video
		respond_to do |format|
			if @video.update(video_params)
				format.html { redirect_to @video, notice: t('video_update_success') }
				format.json { head :no_content }
			else
				format.html { render action: 'edit' }
				format.json { render json: @video.errors, status: :unprocessable_entity }
			end
		end
	end

	def destroy
		@video = @user.videos.find(params[:id])
		authorize @video
		@video.destroy
		respond_to do |format|
			format.html { redirect_to user_videos_path }
			format.json { head :no_content }
		end
	end

	private

	def video_params
		params.require(:video).permit(:user_id, :video)
	end

	def load_user
		@user = User.find(params[:user_id])
	end
end