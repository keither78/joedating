class UserMessagesController < BaseController
	before_filter :load_recipient_and_inbox, only: [:new, :create, :show]

  def index
		@inbox = current_user.inbox
		authorize @inbox
		@user_messages = @inbox.user_messages.where(parent_id: nil).includes(:user, {:sender => [:avatar]}).received.order(created_at: :desc)
  end

  # GET /user_messages/1
  # GET /user_messages/1.json
  def show
		# @user_message.mark_as_read
		@inbox = current_user.inbox
		@user_message = @inbox.user_messages.includes(:user).find(params[:id])
		authorize @user_message
		@children = @user_message.children.includes(:user, :sender).order(created_at: :asc)
		@user_message.mark_as_read
  end

  # GET /user_messages/new
  def new
		@user_message = @inbox.user_messages.new
		authorize @user_message
  end

  # GET /user_messages/1/edit
  def edit
  end

  # POST /user_messages
  # POST /user_messages.json
  def create
		@user_message = @inbox.user_messages.new(user_message_params)
    respond_to do |format|
			if @user_message.add_user_message
				unless @recipient.fake?
					MainMailer.user_sent_inbox_message(@recipient, @user_message.sender).deliver
				else
					MainMailer.send_message_to_fake_user_to_admin(@user_message, @user_message.sender).deliver
				end
        format.html { redirect_to user_messages_path, notice: 'User message was successfully created.' }
        format.json { render action: 'show', status: :created, location: @user_message }
      else
        format.html { render action: 'new' }
        format.json { render json: @user_message.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /user_messages/1
  # PATCH/PUT /user_messages/1.json
  def update
		@inbox = current_user.inbox
		@user_message = @inbox.user_messages.find(params[:id])
    respond_to do |format|
      if @user_message.update(user_message_params)
        format.html { redirect_to @user_message, notice: 'User message was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @user_message.errors, status: :unprocessable_entity }
      end
    end
	end

	def sent_messages
		@user_messages = current_user.sent_user_messages.includes(:user => :avatar)
	end

  # DELETE /user_messages/1
  # DELETE /user_messages/1.json
  def destroy
		@user_message.find(params[:id])
    @user_message.destroy
    respond_to do |format|
      format.html { redirect_to user_messages_url }
      format.json { head :no_content }
    end
  end

  private

	def load_recipient_and_inbox
		@recipient = User.find(params[:user])
		@inbox = @recipient.inbox
	end

	# Never trust parameters from the scary internet, only allow the white list through.
	def user_message_params
		params.require(:user_message).permit(:sender_id, :body, :subject, :user_id, :parent_id)
	end
end
