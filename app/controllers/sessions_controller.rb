class SessionsController < Devise::SessionsController
	skip_before_filter :require_no_authentication

	USER_CHANNEL_KEY = 'user_channel_key'
	USER_ROLE = 'user_role'

	def after_sign_in_path_for(resource)
		if resource.is_a? User
			set_user_role_cookie(resource)
			set_user_cookie(resource)
			set_user_channel_cookie
			set_user_location_cookie(resource)
			user_dashboard_user_path(resource)
		end
	end

	def after_sign_out_path_for(resource)
		if resource.is_a? User
			clear_user_role_cookie
			clear_user_cookie
			clear_user_channel_cookie
			clear_user_location_cookie
		end
		super
	end

	private

	def set_user_role_cookie(user)
		cookies[USER_ROLE] = { value: user.role }
	end

	def set_user_cookie(user)
		cookies['user'] = { value: user.username }
	end

	def set_user_location_cookie(user)
		cookies['location'] = { value: session[:user_info].region_name }
	end

	def clear_user_cookie
		cookies.delete 'user'
	end

	def clear_user_role_cookie
		cookies.delete USER_ROLE
	end

	def clear_user_location_cookie
		cookies.delete 'location'
	end

	def set_user_channel_cookie
		key = current_user.channel_key
		WebsocketRails[key].make_private
		cookies[USER_CHANNEL_KEY] = { value: key, expires: 30.days.from_now }
	end

	def clear_user_channel_cookie
		cookies.delete USER_CHANNEL_KEY
	end
end