class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
	force_ssl if: :ssl_configured?
	before_filter :get_user_info

	theme :resolve_theme

	def store_location
		session[:return_to] = request.url
	end

	def redirect_back_or_default(default)
		redirect_to(session[:return_to] || default)
		session[:return_to] = nil
	end

	private

	def get_user_info
		client_ip = (Rails.env == 'development') ? '98.195.32.48' : request.remote_ip
		if session[:user_info].blank?
			g = GeoIP.new(Rails.root.join('lib/geo/GeoIPCity.dat'))
			user_location = g.city(client_ip)
			session[:user_info] = user_location
		end
	end

	def resolve_theme
		theme = 'blue'

		if cookies[:theme] && params[:theme].blank?
			theme = cookies[:theme]
		end

		if params[:theme]
			if cookies[:theme] && cookies[:theme] != params[:theme]
				cookies.delete :theme
			end
			theme = params[:theme]
		end
		cookies[:theme] = theme
		logger.info "The theme is #{theme}"
		theme
	end

	def ssl_configured?
		!Rails.env.development?
	end
end
