class PagesController < ApplicationController
	layout :choose_layout

  def index
		@users = User.members_and_vips.all_except(current_user).includes([:profile, :avatar]).has_avatar.limit(12)
  end

  def about
		respond_to do |format|
			format.html
			format.js
		end
  end

  def privacy
		respond_to do |format|
			format.js
		end
  end

  def terms
		respond_to do |format|
			format.js
		end
	end

	def compliance
		respond_to do |format|
			format.js
		end
	end

	def check_username
		username = params[:user_name]
		@user = User.where('username = ?', username).first
		if @user.present?
			render json: { error: 'That username is already taken' }, status: 400
		else
			render json: { message: 'Awesome' }, status: 200
		end
	end

	def check_email
		email = params[:user_email]
		@user = User.where('email = ?', email).first
		if @user.present?
			render json: { error: 'That email is already been registered'}, status: 400
		else
			render json: { message: 'Awesome' }, status: 200
		end
	end

	def validate_user
		if params[:field].blank? || params[:value].blank?
			render nothing: true
		else
			@valid = User.validate_field(params[:field], params[:value])
			render json: @valid, status: @valid[:status]
		end
	end

	private

	def choose_layout
		if request.xhr?
			false
		else
			case action_name
				when 'about', 'privacy', 'terms', 'compliance'
					"#{resolve_theme}"
				else
					"#{resolve_theme}/landing"
			end
		end
	end
end
