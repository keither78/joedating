class PhotosController < BaseController
	before_filter :load_user

  def index
		@photos = @user.photos
		authorize @photos
  end

  def new
		@photo = @user.photos.new
		authorize @photo
	end

	def show
		@photo = @user.photos.find(params[:id])
		authorize @photo
	end

	def edit
		@photo = @user.photos.find(params[:id])
		authorize @photo
  end

  def create
		@photo = @user.photos.new(photo_params)
		authorize @photo
    respond_to do |format|
      if @photo.save
        format.html { redirect_to user_photos_path(@user), notice: t('photo_create_success') }
        format.json { render action: 'show', status: :created, location: @photo }
      else
        format.html { render action: 'new' }
        format.json { render json: @photo.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
		@photo = @user.photos.find(params[:id])
		authorize @photo
    respond_to do |format|
      if @photo.update(photo_params)
        format.html { redirect_to @photo, notice: t('photo_update_success') }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @photo.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
		@photo = @user.photos.find(params[:id])
		authorize @photo
		@photo.destroy
    respond_to do |format|
      format.html { redirect_to user_photos_path }
      format.json { head :no_content }
    end
	end

  private

	# Never trust parameters from the scary internet, only allow the white list through.
	def photo_params
		params.require(:photo).permit(:user_id, :image, :avatar)
	end

	def load_user
		@user = User.find(params[:user_id])
	end
end
