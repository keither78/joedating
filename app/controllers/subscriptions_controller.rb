class SubscriptionsController < BaseController
	def new
		plan = Plan.find(params[:plan_id])
		@subscription = plan.subscriptions.build
	end

	def create
		@subscription = Subscription.new(subscription_params)
		if @subscription.save_with_payment
			current_user.update(role: 'vip')
			redirect_to @subscription, notice: 'Thank you for subscribing!'
		else
			render :new
		end
	end

	def show
		@subscription = Subscription.find(params[:id])
	end

	private

	def subscription_params
		params.require(:subscription).permit(:plan_id, :stripe_card_token, :email)
	end
end