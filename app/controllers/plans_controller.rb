class PlansController < BaseController
	skip_before_filter :authenticate_user!, only: :receive
	skip_before_filter :verify_authenticity_token, only: :receive

	def index
		@plans = Plan.order('price')
		@girls = User.members_and_vips.all_except(current_user).includes([:profile, :avatar]).has_avatar.limit(12)
	end

	def receive
		@user = User.where(email: params[:CUST_EMAIL]).first
		method = params[:METHOD]
		sub_status = params[:SUB_STATUS]
		if @user
			@subscription = Subscription.where(email: @user.email).first_or_create
			if method == 'purchase' and sub_status == 'C' # this is to cover just purchase of item no subscriptions
				head :ok
			elsif	method == 'cancel_membership' and sub_status =='C'
				if @subscription.cancel_sub(@user, params)
					@user.update(role: 'member')
					head :ok
				end
			elsif method == 'suspend' and sub_status == 'C'
				if @subscription.suspend_sub(@user, params)
					@user.update(role: 'member')
					head :ok
				end
			elsif method == 'renew' and sub_status == 'A'
				if @subscription.renew_sub(@user, params)
					head :ok
				end
			elsif method == 'void'
				if @subscription.void_sub(@user, params)
					@user.update(role: 'member')
					head :ok
				end
			else
				head :ok
			end
		else
			head :bad_request
		end
	end
end