class MessagesController < BaseController
	#load_and_authorize_resource :message, param_method: :message_params

	# GET /messages
	# GET /messages.json
	def index
		@recipient = User.find(params[:user_id])
		@conversation = Conversation.find_or_create_by(sender: current_user, recipient: @recipient)
		@messages = @conversation.messages
		#@messages = Message.includes(:user).where(user_id: [current_user.id, params[:user_id]], sender_id: [params[:user_id], current_user.id])
		#@user = User.find(params[:user_id])
	end

	# GET /messages/1
	# GET /messages/1.json
	def show
	end

	# GET /messages/new
	def new
	end

	# GET /messages/1/edit
	def edit
	end

	# POST /messages
	# POST /messages.json
	def create
		@message = Message.new(message_params)
		respond_to do |format|
			if @message.save
				format.html { redirect_to @message, notice: 'Message was successfully created.' }
				format.js
				format.json { render action: 'show', status: :created, location: @message }
			else
				format.html { render action: 'new' }
				format.json { render json: @message.errors, status: :unprocessable_entity }
			end
		end
	end

	# PATCH/PUT /messages/1
	# PATCH/PUT /messages/1.json
	def update
		respond_to do |format|
			if @message.update(message_params)
				format.html { redirect_to @message, notice: 'Message was successfully updated.' }
				format.json { head :no_content }
			else
				format.html { render action: 'edit' }
				format.json { render json: @message.errors, status: :unprocessable_entity }
			end
		end
	end

	# DELETE /messages/1
	# DELETE /messages/1.json
	def destroy
		@message.destroy
		respond_to do |format|
			format.html { redirect_to messages_url }
			format.json { head :no_content }
		end
	end

	private

	# Never trust parameters from the scary internet, only allow the white list through.
	def message_params
		params.require(:message).permit(:sender_id, :body, :user_id)
	end
end
