class FriendshipsController < BaseController
	#before_filter :setup_friends, except: :index

	def index
		@friendships = current_user.friendships.accepted.includes(:friend => { :profile => :avatar })
		@pending_friendships = current_user.friendships.pending.includes(:friend => { :profile => :avatar })
		@requested_friendships = current_user.friendships.requested.includes(:friend => { :profile => :avatar })
	end

	def create
		@friendship = current_user.friendships.build(friendship_params)
		respond_to do |format|
			if @friendship.save
				format.html { redirect_to root_url, notice: 'Friend request sent.' }
			else
				format.html { redirect_to root_url, notice: 'Unable to add friend.'}
			end
		end
	end

	def update
		@friendship = current_user.friendships.where(friend_id: params[:id]).first
		respond_to do |format|
			if @friendship.update_attribute(:status, 'accepted')
				format.html { redirect_to root_url, notice: 'Accepted friend request' }
			else
				format.html { redirect_to root_url, notice: 'Unable to accept friend request' }
			end
		end
	end

	def destroy
		@friendship = current_user.friendships.find(params[:id])
		@friendship.destroy
		respond_to do |format|
			format.html { redirect_to root_url, notice: 'Removed friendship' }
		end
		authorize! :destroy, @friendship
	end

	private

	def friendship_params
		params.require(:friendship).permit(:friend_id, :user_id, :status)
	end
end
