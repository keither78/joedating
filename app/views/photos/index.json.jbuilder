json.array!(@photos) do |photo|
  json.extract! photo, :id, :name, :profile_id, :size
  json.url photo_url(photo, format: :json)
end
