# Countdown Plugin
class @CountDownTimer
	oneSecond: 1000
	oneMinute: 1000 * 60
	oneHour: 1000 * 60 * 60
	oneDay: 1000 * 60 * 60 * 24
	newDate: new Date()

	constructor: (selector, timeFrameInMinutes) ->
		@el = $(selector)
		@timeFrame = @newDate.setMinutes(@newDate.getMinutes() + timeFrameInMinutes)
		@startCounting()

	startCounting: ->
		@interval = setInterval (=> @updateCountDown()), @oneSecond
		@updateCountdown()
		return

	stopCounting: ->
		clearInterval(@interval)

	timeLeft: ->
		now = new Date().getTime()
		timeLeft = @timeFrame - now
		if timeLeft < 0
			timeLeft = 0
			@stopCounting()

		minutes = parseInt(((timeLeft % @oneDay) % @oneHour) / @oneMinute)
		seconds = parseFloat((((timeLeft % @oneDay) % @oneHour) % @oneMinute) / @oneSecond)

		{
			minutes: minutes
			seconds: seconds
		}

	updateCountDown: ->
		for section, time of @timeLeft()
			@write(section, time)

	write: (section, number) ->
		sectionElement = @el.find(".#{section}")

		noDecimal = parseInt(number)

		sectionElement.html('')
		sectionElement.append(noDecimal)


$(document).on 'click', '.mainStep1-content a.button', (e) ->
	$('.mainStep1').fadeOut(->
		$('.stepsNext').fadeIn()
	)
	e.preventDefault()

$(document).on 'keypress', '#user_username', (e) ->
	if e.which is 13
		e.preventDefault()
		$('#username_button').click()
	return

$(document).on 'keypress', '#user_password', (e) ->
	if e.which is 13
		e.preventDefault()
		$('#password_button').click()

$(document).on 'keypress', '#user_password_confirmation', (e) ->
	if e.which is 13
		e.preventDefault()
		$('#password_button').click()

$(document).on 'keypress', '#user_email', (e) ->
	if e.which is 13
		e.preventDefault()
		$('#submit_button').click()

$(document).on 'click', '#username_button', (e) ->
	e.preventDefault()
	usernameHolder = $('#user_username')
	username = usernameHolder.val()
	$('.error').remove()
	if username is ''
		usernameHolder.after('<span class="error">Please enter a username</span>')
		return false

	postData = { }
	postData['field'] = 'username'
	postData['value'] = username
	request = $.ajax(
		type: 'POST'
		url: '/validate_user'
		data: postData
		dataType: 'json'
	)

	request.done((data) ->
		$('.step1').fadeOut(->
			$('.step2').fadeIn(->
				$('.mainSteps li span').removeClass('cur')
				$('.mainSteps li.step2_indicator span').addClass('cur')
			)
		)
	)

	request.fail((data) ->
		response = JSON.parse(data.responseText)
		$(usernameHolder).after '<span class="error">' + response.username + '</span>'
	)
	return

$(document).on 'click', '#password_button', (e) ->
	e.preventDefault()
	passwordHolder = $('#user_password')
	password = passwordHolder.val()
	passwordConfirmHolder = $('#user_password_confirmation')
	passwordConfirm = passwordConfirmHolder.val()
	$('.error').remove()

	if password is ''
		passwordHolder.after('<span class="error">Please enter a password</span>')
		return false

	unless password.length >= 8 and password.length <= 128
		passwordHolder.after('<span class="error">Must be between 8 and 128</span>')
		return false

	unless password is passwordConfirm
		passwordConfirmHolder.after('<span class="error">Passwords to not match</span>')
		return false
	else
		$('.step2').fadeOut(->
			$('.step3').fadeIn(->
				$('.mainSteps li span').removeClass('cur')
				$('.mainSteps li.step3_indicator span').addClass('cur')
			)
		)

$(document).on 'click', '#submit_button', (e) ->
	e.preventDefault()
	emailHolder = $('#user_email')
	email = emailHolder.val()
	$('.error').remove()

	if email is ''
		emailHolder.after('<span class="error">Please enter your email</span>')
		return false

	postData = { }
	postData['field'] = 'email'
	postData['value'] = email
	request = $.ajax(
		type: 'POST'
		url: '/validate_user'
		data: postData
		dataType: 'json'
	)

	request.done((data) ->
		$('#new_user').submit()
	)

	request.fail((data) ->
		response = JSON.parse(data.responseText)
		$(emailHolder).after '<span class="error">' + response.email + '</span>'
	)
	return

$(document).ready ->
	# by minutes
	window.timer = new CountDownTimer('.timer', 12)