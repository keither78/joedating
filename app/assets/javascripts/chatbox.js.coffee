#TODO need to refactor this yet again to work with turbolinks

escapeStr = (str) ->
	if str
		return str.replace(/([ #;?%&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
	str

playSound = ->
	sound = $('#beepAudio')[0]
	sound.src = sound.src
	sound.play()

openMainModel = (titleText, modelHtml) ->
	title 	= 	'.modal-title'
	content	=		'.modal-body-content'
	modal	=			'#main-modal'

	$(title).html(titleText)
	$(content).html(modelHtml)
	$(modal).addClass('tiny')
	$(modal).foundation('reveal', 'open')

@ChatBox = (options, actions) ->
	if this instanceof ChatBox
		self = this
		defaults =
			url: ''

		@settings = $.extend(defaults, options)
		@actions = $.extend(
			listen: @settings.url
		, actions)

		@chats = {}

		self.setup()

		$(document).on 'click', '.chatbox', (e) ->
			e.preventDefault()
			return false

		$(document).on 'click', '.chatbox .toggle_chat', (e) ->
			$(this).parents('.tab-selected').click()

		$(document).on 'click', '.im_chat_request', (e) ->
			e.preventDefault()
			userid = $(this).data('userid')
			url = $(this).attr('href') + "?user_id=#{userid}"
			username = $(this).data('username')
			chatbox = self._createChatbox(url, username)
			unless chatbox.parents('.chatbox-tab').data('state') is 'active'
				store.set self.username + '-activeTab', username
			return


		$(document).on 'submit', '.user_input_form', (e) ->
			e.preventDefault()
			username = $(this).parents('.chatbox-tab').data('username')
			if self.userRole isnt 'vip'
				openMainModel(
					'Do you want to upgrade now?',
					'<p>Communication with members is a function reserved for VIP members only</p><ul class="inline-list"><li><a href="/plans" class="button tiny radius">Yes</a></li><li><a href="#" class="button tiny radius close-modal-window">No</a></li></ul>')
				$(this)[0].reset()
				return false
			message = $(this).find('.user-input').val()
			if message
				message = message.toLowerCase()
				self.send username, message
				self._handleMsg(username, message)

	else
		ChatBox.init options
	return

$.extend ChatBox::,
	setup: ->
		@initTabBar()
		@username = $.cookie('user')
		@userRole = $.cookie('user_role')
		@userLocation = $.cookie('location')
		@storage() if @username

	storage: ->
		self = this
		chatstore = store.get(@username + '-chats')
		@chatstore = chatstore or {}
		$.each @chatstore, (username) ->
			$('#private_messages').append(self.chatstore[username])

		activeTab = store.get(@username + '-activeTab')
		if activeTab and activeTab of @chatstore
			parent = $('#chat_window_' + escapeStr(activeTab))
			if parent.length
				msglog = parent.find('.chatbox_body')
				parent.click()
				parent.find('.panel-body').scrollTop(msglog[0].scrollHeight)
		return

	_createMemberChatBox: (url) ->
		self = this
		username = $.cookie('messaging_user')
		if !$.cookie('messaging_user')
			ChatBox.get(url, '', 'html',
				((result) ->
					$('#private_messages').append result
					username = $(result).data('username')
					tab = $('#chat_window_' + escapeStr(username))
					chatbox = $('<div/>').append(tab.clone())
					self._store(username, chatbox)
					tab.data('state', 'minimized')
					tab.click()
					self._autoResponse(username)
				),(error) ->
					console.log error
			)

	_createChatbox: (url, username) ->
		self = this
		chatbox_id = 'chat_window_' + escapeStr(username)
		unless(chatbox = $('#' + chatbox_id)).length
			ChatBox.post(url, '', 'html',
				((result) ->
					$('#private_messages').append result
					tab = $('#' + chatbox_id)
					chatbox = $('<div/>').append(tab.clone())
					self._store(username, chatbox)
					tab.data('state', 'minimized')
					tab.click()
				)
			)
			@chats[username] = chatbox
		else if chatbox.data('state') is 'closed'
			chatbox.css('display', '').removeClass('tab-selected').data 'state', 'minimized' if chatbox.css('display') is 'none'
			chatbox_clone = $('<div/>').append($(chatbox).clone())
			$('#private_messages').append(chatbox)
			@_store username, chatbox_clone
			unless $('#private_messages .tab-selected').length
				chatbox.click()
		chatbox

	_store: (username, msg) ->
		return unless msg.html.length
		@chatstore = {} unless @chatstore
		unless username of @chatstore
			@chatstore[username] = []
		else @chatstore[username].shift() if @chatstore[username].length > 300

		if(store.get @username + '-chats')
			@chatstore[username].pop()

		@chatstore[username].push msg.html()

		store.set @username + '-chats', @chatstore
		return

	_handleMsg: (username, message) ->
		if $.ContainsAny(message, ['hello', 'hi', 'howdy', 'sup', 'what\'s up', 'hey baby', 'hey sexy', 'hello sexy', 'hello baby', 'sup babe'])
			messageArray = [
				'Hello to you too!'
				'Why hello there!'
				'Hey what\'s up'
				'Hey! I\'m so glad you messaged me!'
			]
			@_botresponse(username, messageArray)
		else if $.ContainsAny(message, ['good morning'])
			messageArray = [
				'Good morning to you too.  How are you this morning?'
				'Yeah, it is a good morning'
			]
			@_botresponse username, messageArray
		else if $.ContainsAny(message, ['fine, thanks', 'fine', 'fine thanks', 'not to bad', 'doing good', 'good'])
			messageArray = [
				'That is good to hear!'
			]
			@_botresponse username, messageArray
		else if $.ContainsAny(message, ['want to fuck', 'let\'s fuck', 'lets fuck', 'wanna fuck'])
			messageArray = [
				'Whoaa! there cowboy, let\'s get to know each other a little first ok?'
				'Strait to the point eh?'
			]
			@_botresponse username, messageArray
		else if $.ContainsAny(message, ['yep'])
			messageArray = [
				'Yep'
			]
			@_botresponse username, messageArray
		else if $.ContainsAny(message, ['you suck', 'bitch', 'fuck you'])
			messageArray = [
				'No need to be rude'
				'Look asshole I don\'t need to take any shit from you'
			]
			@_botresponse username, messageArray
		else if $.ContainsAny(message, ['I\'m sorry', 'sorry', 'I apologize'])
			messageArray = [
				'That\'s ok, just don\'t let it happen again'
			]
			@_botresponse username, messageArray
		else if $.ContainsAny(message, ['want to meet', 'let\'s meet', 'want to go on a date'])
			messageArray = [
				'That sounds good, though let us get to know each other a little better here first'
			]
			@_botresponse username, messageArray
		else if $.ContainsAny(message, ['ok'])
			messageArray = [
				'Great!'
			]
			@_botresponse username, messageArray
		return

	_autoResponse: (username) ->
		self = this
		n = Math.floor(Math.random() * 2)
		switch n
			when 0
				messageArray = [
					'Hey there sexy!'
					'You wanna party?'
					'Hey wanna get to know each other'
				]
				locationArray = [
					('Hey! u in ' + self.userLocation + '?')
				]
				self._botresponse(username, messageArray, false)
				setTimeout (->
					self._botresponse(username, locationArray)
				), 1000
			else
				advert = '<li>' + username + ' would like to see your photos!</li>'
				advert += '<li><a href="/plans" class="btn chat_promo_btn promo_btn_1">Chat with her!</a></li>'
				advert += '<li><a href="#" class="btn chat_promo_btn promo_btn_2 tab-close">Not Now</a></li>'
				self._botresponse(username, advert, false, true)


	_botresponse:	(username, message, timed=true, advertisment=false) ->
		self = this
		r = message[Math.floor(Math.random() * message.length)] unless advertisment
		parent = $('#chat_window_' + escapeStr(username))
		if timed
			setTimeout (->
				botPre = '<span class="message">' + username + ' is writing.. <i class="glyphicon glyphicon-pencil"></i></span>'
				parent.find('.chatbox_info').html(botPre)
			), 800
			setTimeout (->
				parent.find('.chatbox_body').append('<li>' + username + ': ' + r + '</li>')
				playSound()
				parent.find('.chatbox_info').html ""
				msglog = parent.find('.chatbox_body')
				parent.find('.panel-body').scrollTop(msglog[0].scrollHeight)
				chatbox = $('<div/>').append(parent.clone())
				self._store username, chatbox
			), 2800
		else if advertisment
			parent.find('.chatbox_body').append(message)
			playSound()
			parent.find('.user-input').prop('disabled', true)
			chatbox = $('<div/>').append(parent.clone())
			self._store username, chatbox
		else
			parent.find('.chatbox_body').append('<li>' + username + ': ' + r + '</li>')
			playSound()
			chatbox = $('<div/>').append(parent.clone())
			self._store username, chatbox
		return


	initTabBar: ->
		self = this

		$(document).on 'click', '.chatbox-tab', ->
			return self.activateTab.call(self, $(this))

		$(document).on 'click', '.tab-close', (e) ->
			e.preventDefault()
			return self.closeTab.call(self, $(this))

		$(document).on 'click', '.promo_btn_1', ->
			window.location = $(this).attr('href')

	activateTab: (tab) ->
		chatbox = tab.find('.chatbox') or false

		if tab.data('state') isnt 'active'
			$('#private_messages').children().not(tab).each ->
				self = $(this)
				self.removeClass('tab-selected')
				if self.data('state') isnt 'closed'
					self.data 'state', 'minimized'
					ichatbox = self.find('.chatbox')
					ichatbox.addClass('hidden')# if chatbox.length

			chatbox.removeClass('hidden') if chatbox and chatbox.hasClass('hidden')
			tab.addClass('tab-selected').data 'state', 'active'
			store.set @username + '-activeTab', username if chatbox and (username = tab.data('username'))
		else
			tab.removeClass('tab-selected').data 'state', 'mininized'
			chatbox.addClass('hidden') if chatbox and chatbox.not('hidden')
			store.set @username + '-activeTab'

	closeTab: (tab) ->
		tab = tab.parents('.chatbox-tab')
		tab.css('display', 'none').removeClass('tab-selected').data 'state', 'closed'
		username = tab.data('username')
		delete @chatstore[username]
		store.set @username + '-chats', @chatstore
		@_setExpiringCookie()
		if($.cookie('messaging_user'))
			$.removeCookie('messaging_user', { path: '/'} )

	_setExpiringCookie: (time = 10) ->
		date = new Date()
		minutes = time
		date.setTime(date.getTime() + (minutes * 60 * 1000))
		$.cookie('waitForUser', 'waiting', { expires: date })

	send: (username, message) ->
		parent = $('#chat_window_' + escapeStr(username))
		msglog = parent.find('.chatbox_body')
		userMessage = '<li>You: '+ message + '</li>'
		msglog.append(userMessage)
		parent.find('form')[0].reset()
		chatbox = $('<div/>').append(parent.clone())
		parent.find('.panel-body').scrollTop(msglog[0].scrollHeight)
		@_store username, chatbox

@ChatBox.client = null

@ChatBox.init = (options, actions) ->
	ChatBox.client = new ChatBox(options, actions) unless ChatBox.client
	ChatBox.client

@ChatBox.post = (url, data, dataType, successFunc, failureFunc) ->
	ChatBox.request(url, 'POST', data, dataType, successFunc, failureFunc)
	return

@ChatBox.get = (url, data, dataType, successFunc, failureFunc) ->
	ChatBox.request(url, 'GET', data, dataType, successFunc, failureFunc)
	return

@ChatBox.request = (url, type, data, dataType, successFunc, failureFunc) ->
	$.ajax(
		url: url,
		type: type,
		dataType: dataType,
		cache: false,
		timeout: 299000
	).done((result) ->
		successFunc result
		return
	).fail (jqXHR, textStatus) ->
		failureFunc textStatus
		return
	return


$(document).ready ->
	im = new ChatBox.init()
	rollCookie = $.cookie('user_role')
	waitCookie = $.cookie('waitForUser')
	if rollCookie is 'member' and not waitCookie
		setTimeout (->
			im._createMemberChatBox('/random_user')
		), 4000