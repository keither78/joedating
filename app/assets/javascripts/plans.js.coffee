getName = (name) ->
	fieldName = undefined

	switch name
		when 'cust_fname'
			fieldName = 'First Name'
		when 'cust_lname'
			fieldName = 'Last Name'
	return fieldName

$(document).on 'submit', '#plan-form', (e) ->
	errors = 0
	errormsg = []
	unless $('input:radio:checked').length
		errors++
		errormsg.push 'Please select at least one plan to upgrade'

	$('#plan-form :input').map ->
		unless $(this).val()
			name = getName($(this).attr('name'))
			errors++
			errormsg.push (name + ' is required')
		return
	if errors > 0
		$('#error_explanation').html ''
		$('#error_explanation').append('<h4>' + errors + ' errors prohibited this form from submitting</h4>')
		el = $.map(errormsg, (val, i) ->
			'<li>' + val + '</li>'
		)
		$('#error_explanation').append el.join('')
		false