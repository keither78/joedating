#class @GoogleAnalytics
#
#	@load: ->
#		window._gaq = []
#		window._gaq.push ["_setAccount", GoogleAnalytics.analyticsId()]
#
#		ga = document.createElement('script')
#		ga.type = 'text/javascript'
#		ga.async = true
#		ga.src = ((if 'https:' is document.location.protocol then 'https://ssl' else 'http://www')) + '.google-analytics.com/ga.js'
#		firstScript = document.getElementsByTagName('script')[0]
#		firstScript.parentNode.insertBefore ga, firstScript
#
#	@analyticsId: ->
#		'UA-54647014-1'
#
#GoogleAnalytics.load()
#
#$.extend googleAnalytics:
#		trackEvent: (args) ->
#			defaultArgs = category: 'Unspecified', action: 'Unspecified', nonInteractive: false
#			args = $.extend defaultArgs, args
#			window._gaq.push(['_trackEvent', args.category, args.action, args.label, args.value, args.nonInteractive])
#			console.log 'Tracked event'
#
#defaultOptions =
#	categoryAttribute: 'data-ga-category'
#	actionAttribute: 'data-ga-action'
#	labelAttribute: 'data-ga-label'
#	valueAttribute: 'data-ga-value'
#	noninteractiveAttribute: 'data-ga-noneinteractive'
#	useLabel: false
#	useValue: false
#	useEvent: false
#	event: 'click'
#	valid: (elem, e) ->
#		true
#	complete: (elem, e) ->
#	category: 'Unspecified'
#	action: 'Unspecified'
#	label: ''
#	value: ''
#	nonInteractive: false
#
#$.fn.gaTrackEvent = (options) ->
#	options = $.extend({}, defaultOptions, options)
#	@each ->
#		element = $(this)
#		element.attr options.categoryAttribute, options.category
#		element.attr options.actionAttribute, options.action
#		element.attr options.labelAttribute, options.label if options.useLabel is true and options.label isnt ''
#		element.attr options.valueAttribute, options.value if options.useValue is true and options.value isnt ''
#		element.attr options.noninteractiveAttribute, 'true' if options.nonInteractive is true
#		element.gaTrackEventUnobtrusive options
#		return
#
#$.fn.gaTrackEventUnobtrusive = (options) ->
#	options = $.extend({}, defaultOptions, options)
#	@each ->
#		_this = $(this)
#
#		callTrackEvent = ->
#			category = _this.attr(options.categoryAttribute)
#			action = _this.attr(options.actionAttribute)
#			label = _this.attr(options.labelAttribute)
#			value = _this.attr(options.valueAttribute)
#			nonInteractive = (_this.attr(options.noninteractiveAttribute) is 'true')
#			args =
#				category: category
#				action: action
#				nonInteractive: nonInteractive
#
#			if options.useLabel and options.useValue
#				args.label = label
#				args.value = value
#			else args.label = label if options.useLabel
#
#			$.googleAnalytics.trackEvent args
#			return
#
#		if options.useEvent is true
#			constructedFunction = (e) ->
#				if options.valid(_this, e) is true
#					callTrackEvent()
#					options.complete _this, e
#				return
#			_this.bind options.event, constructedFunction
#		else
#			callTrackEvent()
#		return
#	return