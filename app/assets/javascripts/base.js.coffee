jQuery.fn.outerHTML = ->
	(if (this[0]) then this[0].outerHTML else "")

jQuery.isSubstring = (haystack, needle) ->
	haystack.indexOf(needle) isnt -1

jQuery.ContainsAny = (str, items) ->
	for i of items
		item = items[i]
		return true if str.indexOf(item) > -1
	false

UserChannel = (->
	dispatcher = ''
	channelName = ''
	cookieName = 'user_channel_key'

	init = (globalDispatcher) ->
		dispatcher = globalDispatcher
		dispatcher.on_open = connect
		channelName = $.cookie(cookieName)

	connect = ->
		if channelName
			getChannel channelName
		else
			getKey()

	getChannel = (key) ->
		channel = dispatcher.subscribe_private(key)
		channel.on_success = ->

		channel.on_failure = (reason) ->
			$.removeCookie cookieName
			console.log 'Authorization failed because ' + reason.message

	getKey = ->
		dispatcher.bind 'user.key', (key) ->
			$.cookie cookieName, key, expires: 30

			getChannel(key)

		console.log 'got key'
		dispatcher.trigger 'user.get_channel_key', {}
	init: init
)()


UserMonitor = (->
	dispatcher = ''
	channel = ''
	onlineUsers = ''
	$userCountContainer = ''

	init = (globalDispatcher) ->
		dispatcher = globalDispatcher
		channel = dispatcher.subscribe('online_users')
		channel.bind 'has_been_seen', userOnline
		channel.bind 'has_left', userOffline
		setOnlineUserCount()

	setOnlineUserCount = ->
		$userCountContainer = $('#online-user-count')
		onlineUsers = parseInt($userCountContainer.html(), 10)  if $userCountContainer.length

	userOnline = (user) ->
		$onlineUser = $('.user-online[data-user-id=' + user.id + ']')
		$onlineUser.addClass 'online'
		$onlineUser.removeClass 'offline'
		$onlineUser.html 'Online'
		updateOnlineUserCount ++onlineUsers  if $userCountContainer.length

	userOffline = (user) ->
		$offlineUser = $('.user-online[data-user-id=' + user.id + ']')
		$offlineUser.removeClass 'online'
		$offlineUser.addClass 'offline'
		$offlineUser.html 'Offline'
		updateOnlineUserCount --onlineUsers  if $userCountContainer.length

	updateOnlineUserCount = (count) ->
		$userCountContainer.html count

	init: init
)()

moveToBottom = (div) ->
	$(div).animate({scrollTop: $(div)[0].scrollHeight}, 800)

playvideo = (theVideo) ->
	pauseTime = 2
	current = undefined
	getToPlay = ->
		current = Math.floor(@currentTime())
		if current is pauseTime
			@pause()
			theVideo.off 'timeupdate', getToPlay
		return
	theVideo.on 'timeupdate', getToPlay
	theVideo.play()
	return

acceptedRoles = [
	'admin'
	'vip'
]

jQuery.colorbox.settings.maxWidth = '95%';
jQuery.colorbox.settings.maxHeight = '95%';

$(document).bind 'contextmenu', (e) ->
	e.preventDefault()

$(document).on 'click', '.close-modal-window', (e) ->
	e.preventDefault()
	$('#main-modal').foundation('reveal', 'close')
	$('#main-modal').removeClass('tiny')

$(document).on 'page:change', ->
	ga('send', 'pageview', window.location.pathname)

$(document).ready ->
	ga('send', 'pageview', window.location.pathname)

	$('.track-click').on 'click', (e) ->
		category = $(this).data('ga-category')
		action = $(this).data('ga-action')
		label = $(this).data('ga-label')
		ga('send', 'event', category, action, label)

	$('.lightbox').on 'click', (e) ->
		e.preventDefault()
		distort = false
		url = $(this).data('href')
		userRole = $.cookie('user_role')
		if $.inArray(userRole, acceptedRoles) is -1
			distort = true
		$.colorbox(
			href: url
			title: ->
				if distort
					return '<a href="/plans">Click here to see the full photo</a>'
			onComplete: ->
				if distort
					parent = $('#cboxLoadedContent')
					parent.css('height': 'auto')
					parent.find('img').pixelate()
					parent.append('<div class="pixel_message"><h2>Full resolution images are available to VIP members only <a href="/plans">Click here to upgrade!</a></h2></div>')
		)

	$('.videobox').on 'click', (e) ->
		e.preventDefault()
		url = $(this).data('href')
		userRole = $.cookie('user_role')
		if $.inArray(userRole, acceptedRoles) is -1
			startTime = 0
			endTime = 4
			$.colorbox(
				href: url
				title: ->
					return '<a href="/plans">Click here to see the full video</a>'
				onComplete: ->
					video = $('video')
					parentContainer = video.parents('#cboxLoadedContent')
					player = new MediaElementPlayer(video,
						success: (mediaElement, domObject) ->
							mediaElement.addEventListener 'timeupdate', (e) ->
								if mediaElement.currentTime >= endTime
									@pause()
									console.log parentContainer
									parentContainer.append('<div class="video_message"><h2>To see the full video upgrade now! <a href="/plans">Click here to upgrade</a></h2></div>')
									mediaElement.setCurrentTime(startTime)
					)
					player.play()
			)
		else
			$.colorbox(
				href: url
				onComplete: ->
					video = $('video')
					player = new MediaElementPlayer(video, {})
					player.play()
			)