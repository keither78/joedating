module ApplicationHelper

	def human_boolean(boolean)
		case boolean
			when TRUE
				content_tag(:span, 'Yes', class: 'label label-success')
			else
				content_tag(:span, 'No', class: 'label label-danger')
		end
	end

	def pagination_links(collection, options = {})
		# options[:renderer] ||= BootstrapPaginationHelper::LinkRenderer
		options[:renderer] ||= FoundationPaginationHelper::LinkRenderer
		will_paginate(collection, options)
	end

	def flash_class(level)
		case level.to_sym
			when :notice then
				'alert-info'
			when :success then
				'alert-success'
			when :error then
				'alert-danger'
			when :alert then
				'alert-warning'
		end
	end

	def icon_link(link_text, link_path, icon, link_to_options = {})
		link_to(link_path, link_to_options) { icon(icon) + " #{link_text}" }
	end

	def nav_link(link_text, link_path, icon)
		class_name = current_page?(link_path) ? 'active' : ''
		content_tag(:li, class: class_name) do
			link_to link_path do
				icon(icon) + " #{link_text}"
			end
		end
	end

	def flash_normal
		render 'shared/flash'
	end

	def resource_name
		:user
	end

	def resource
		@resource ||= User.new
	end

	def devise_mapping
		@devise_mapping ||= Devise.mappings[:user]
	end

	private

	def icon(icon_class)
		content_tag(:span, nil, class: "fa fa-#{icon_class}")
	end
end
