module FriendshipHelper

	# Example <%= image_link_to "image_path", url_helper, 'you really need to see this' { size: '210x60', alt: 'Somecool Alt'}, { title: 'Something cool', rel: 'tooltip', relative: true } %>  - You get the point
	def image_link_to(image_path, url, image_tag_options = {}, link_to_options = {}, &block)
		link_to url, link_to_options do
			content_tag(:figure) do
				concat image_tag image_path, image_tag_options
				concat content_tag(:figcaption, &block)
			end
		end
	end

	def friendship_status(user, friend)
		friendship = user.check_friendships(friend)
		if friendship.nil?
			return content_tag(:div, class: 'row') do
				concat content_tag(:span, link_to('Add Friend', friendships_path(friendship: {friend_id: friend, status: 'requested'}), method: :post, class: 'btn btn-primary'), class: 'col-md-2')
				#concat content_tag(:span, class: 'col-md-2') do
				#	link_to 'Add Friend', friendships_path(friendship: {friend_id: friend, status: 'requested'}), method: :post, class: 'btn btn-primary'
				#end
				concat content_tag(:span, ' You are not friends (yet)', class: 'col-md-2')
			end
		end

		case friendship.status
			when 'requested'
				content_tag(:span, 'You have already requested friendship')
			when 'pending'
				content_tag(:span, 'This user has already requested you as a friend')
			when 'accepted'
				content_tag(:span, 'You are friends')
			else
				# type code here
		end
	end
end
